"""Test DSFRPlugin integration."""

import pytest
import os
import shutil
import subprocess
from bs4 import BeautifulSoup


PROJECT_PATH = os.path.join(os.path.dirname(__file__), "../fixtures/projects")
DOCS_PATH = os.path.join(os.path.dirname(__file__), "../../docs")


@pytest.mark.parametrize(
    "project_name, check",
    [
        ("simple", True),
        ("simple-with-plugin", True),
        ("awesome-pages", True),
        ("swagger-ui-tag", True),
        ("kroki", True),
    ],
)
class TestIntegrationProjects:
    """Test DSFRPlugin integration."""

    def test_integration_project(self, project_name: str, check: bool) -> None:
        """Test DSFRPlugin integration with other plugins."""
        base_path = f"{PROJECT_PATH}/{project_name}"
        mkdocs_file = f"{base_path}/mkdocs.yml"
        generated_site = f"{base_path}/site"
        index_file = f"{generated_site}/index.html"

        if check:
            self.__copy_docs_folder(base_path)
            self.__build_doc(mkdocs_file)
            self.__title_exists(index_file, project_name)
            self.__delete_docs_folder(base_path)
            self.__delete_generated_site(generated_site)

    def __copy_docs_folder(self, base_path: str) -> None:
        command = f"cp -r {DOCS_PATH} {base_path}"
        result = subprocess.run(command, shell=True, capture_output=True, text=True)
        assert result.returncode == 0

    def __build_doc(self, mkdocs_file: str) -> None:
        command = f"TZ=UTC mkdocs build -f {mkdocs_file}"
        result = subprocess.run(command, shell=True, capture_output=True, text=True)
        assert result.returncode == 0

    def __title_exists(self, index_file: str, project_name: str) -> None:
        with open(index_file, "r") as file:
            html_content = file.read()
        soup = BeautifulSoup(html_content, "html.parser")
        target_element = str(soup.find("head").find("title"))
        assert target_element == f"<title>MkDocs DSFR - {project_name}</title>"

    def __delete_docs_folder(self, base_path: str) -> None:
        docs_folder = base_path + "/docs"
        if os.path.exists(docs_folder):
            shutil.rmtree(docs_folder)

    def __delete_generated_site(self, generated_site: str) -> None:
        if os.path.exists(generated_site):
            shutil.rmtree(generated_site)

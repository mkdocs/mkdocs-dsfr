"""Test DSFRPlugin class."""

import unittest
import re
from mkdocs_dsfr.plugin.plugin import DSFRPlugin


class TestDSFRPlugin(unittest.TestCase):
    """Test DSFRPlugin class methods."""

    def test_correct_replace_component_not_none(self) -> None:
        """Test DSFRPlugin replace_component method not None result."""
        yaml_example = "```dsfr-plugin-notice\nDDescription\n```"
        match_example = re.match(r"```dsfr-plugin(?:-(\w+))?\n(.*?)\n```", yaml_example)
        dsfr_plugin = DSFRPlugin()
        result = dsfr_plugin.replace_component(match_example)
        assert result is not None

    def test_incorrect_replace_component_is_empty_string(self) -> None:
        """Test DSFRPlugin replace_component method return empty string."""
        yaml_example = "```dsfr-plugin-notice\nContent\n```"
        match_example = re.match(r"```dsfr-plugin(?:-(\w+))?\n(.*?)\n```", yaml_example)
        dsfr_plugin = DSFRPlugin()
        result = dsfr_plugin.replace_component(match_example)
        self.assertEqual(result, "")

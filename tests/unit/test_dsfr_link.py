"""Test DSFRLink class."""

import unittest
from mkdocs_dsfr.plugin.link import DSFRLink


HTML_INPUT = '<a download="download" href="test-download.txt">test-download</a>'
EXPECTED_HTML_OUTPUT = """<a class="fr-link--download fr-link" download="download" href="test-download.txt">
 test-download
</a>
"""


class TestDSFRLink(unittest.TestCase):
    """Test DSFRLink class methods."""

    def test_update(self) -> None:
        """Test DSFRLink update method works."""
        dsfr_link = DSFRLink()
        self.assertEqual(dsfr_link.update(HTML_INPUT), EXPECTED_HTML_OUTPUT)

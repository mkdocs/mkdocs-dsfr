"""Test DSFRTable class."""

import unittest
from mkdocs_dsfr.plugin.table import DSFRTable
from mkdocs_dsfr.plugin.config import DSFRConfig


HTML_INPUT = "<table><tr><th>Content</th></tr><tr><td>Test</td></tr></table>"
EXPECTED_HTML_OUTPUT = """<div class="fr-table">
 <table>
  <tr>
   <th>
    Content
   </th>
  </tr>
  <tr>
   <td>
    Test
   </td>
  </tr>
 </table>
</div>
"""
EXPECTED_HTML_WITH_CONFIG_OUTPUT = """<div class="fr-table fr-table--bordered fr-table--no-scroll fr-table--layout-fixed fr-table--green-emeraude">
 <table>
  <tr>
   <th>
    Content
   </th>
  </tr>
  <tr>
   <td>
    Test
   </td>
  </tr>
 </table>
</div>
"""


class TestDSFRTable(unittest.TestCase):
    """Test DSFRTable class methods."""

    def test_update(self) -> None:
        """Test update tables."""
        config = DSFRConfig()
        dsfr_table = DSFRTable(config)
        self.assertEqual(dsfr_table.update(HTML_INPUT), EXPECTED_HTML_OUTPUT)

    def test_update_config(self) -> None:
        """Test update DSFRConfig class for tables."""
        config = DSFRConfig()
        config.table_bordered = True
        config.table_no_scroll = True
        config.table_layout_fixed = True
        config.table_color = "green-emeraude"
        dsfr_table = DSFRTable(config)
        self.assertEqual(dsfr_table.update(HTML_INPUT), EXPECTED_HTML_WITH_CONFIG_OUTPUT)

"""Test DSFRPlugin class for components."""

import pytest
import os
from mkdocs.config.defaults import MkDocsConfig
from mkdocs_dsfr.plugin.plugin import DSFRPlugin


DATA_PATH = os.path.join(os.path.dirname(__file__), "../fixtures/data")


@pytest.mark.parametrize(
    "test_name, check",
    [
        ("card", True),
        ("tab", True),
        ("alert", True),
        ("badge", True),
        ("notice", True),
        ("callout", True),
        ("accordion", True),
        ("icon", True),
        ("picto", True),
        ("tile", True),
        ("highlight", True),
        ("quote", True),
    ],
)
class TestDSFRPluginComponents:
    """Test DSFRPlugin class for components."""

    def test_on_page_markdown(self, test_name: str, check: bool) -> None:
        """Test on_page_markdown method for every components."""
        if check:
            markdown_input_path = f"{DATA_PATH}/{test_name}-markdown-input.md"
            markdown_result_path = f"{DATA_PATH}/{test_name}-markdown-result.md"

            with open(markdown_input_path, "r") as file:
                markdown_input = file.read()
            with open(markdown_result_path, "r") as file:
                markdown_result = file.read()

            dsfr_plugin = DSFRPlugin()
            config = MkDocsConfig()
            config.load_dict({"site_url": "https://localhost:8000/"})
            assert dsfr_plugin.on_page_markdown(markdown_input, None, config, None) == markdown_result

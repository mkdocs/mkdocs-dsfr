"""Test DSFRMedia class."""

import unittest
from mkdocs_dsfr.plugin.media import DSFRMedia


class TestDSFRMedia(unittest.TestCase):
    """Test DSFRMedia class methods."""

    def test_simple_update(self) -> None:
        """Test DSFRMedia update method works."""
        html_input = '<img alt="example" src="../assets/images/test.png"/>\n'
        expected_html_output = '<img alt="example" class="fr-responsive-img" src="../assets/images/test.png"/>\n'
        dsfr_media = DSFRMedia()
        self.assertEqual(dsfr_media.update(html_input), expected_html_output)

    def test_emoji_not_updating(self) -> None:
        """Test emoji not updated by DSFRMedia update method."""
        html_input = '<img alt="😄" class="emojione" src="https://cdnjs.cloudflare.com/ajax/libs/emojione/2.2.7/assets/svg/1f604.svg" title=":smile:"/>\n'
        expected_html_output = html_input
        dsfr_media = DSFRMedia()
        self.assertEqual(dsfr_media.update(html_input), expected_html_output)

    def test_shields_io_not_updating(self) -> None:
        """Test shields.io not updated by DSFRMedia update method."""
        html_input = '<img alt="badge" src="https://img.shields.io/badge/any%20text-you%20like-blue"/>\n'
        expected_html_output = html_input
        dsfr_media = DSFRMedia()
        self.assertEqual(dsfr_media.update(html_input), expected_html_output)

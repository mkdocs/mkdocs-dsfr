"""Test DSFRUtils class."""

import unittest
from mkdocs_dsfr.plugin.utils import DSFRUtils


EXPECTED_HTML_OUTPUT = """<div class="fr-notice fr-notice--info">
  <div class="fr-container">
    <div class="fr-notice__body">
      <p class="fr-notice__title">
        test
      </p>
    </div>
  </div>
</div>"""


class TestDSFRUtils(unittest.TestCase):
    """Test DSFRUtils class methods."""

    def test_parse_yaml_correct(self) -> None:
        """Test to parse a valid yaml data."""
        correct_yaml_str = """
        name: John
        age: 30
        city: New York
        list:
          - element1
          - element2
        """
        dsfr_utils = DSFRUtils()
        result = dsfr_utils.parse_yaml(correct_yaml_str)

        self.assertEqual(result["name"], "John")
        self.assertEqual(result["age"], 30)
        self.assertEqual(result["city"], "New York")
        self.assertEqual(result["list"][0], "element1")
        self.assertEqual(result["list"][1], "element2")

    def test_parse_yaml_incorrect(self) -> None:
        """Test to parse an invalid yaml data."""
        incorrect_yaml_str = "{\nname: John\n  age:: 30\n}"

        dsfr_utils = DSFRUtils()
        result = dsfr_utils.parse_yaml(incorrect_yaml_str)
        self.assertEqual(result, "")

    def test_render_template(self) -> None:
        """Test to render a template."""
        dsfr_utils = DSFRUtils()
        template_data = {
            "data": {
                "title": "test"
            }
        }
        result = dsfr_utils.render_template("notice", template_data)
        self.assertEqual(result, EXPECTED_HTML_OUTPUT)

    def test_nonexistent_render_template(self) -> None:
        """Test to render a non-existent template."""
        dsfr_utils = DSFRUtils()
        data = {}
        result = dsfr_utils.render_template("nonexistent", data)
        self.assertEqual(result, "")

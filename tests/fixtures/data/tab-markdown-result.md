# Test

<div class="fr-tabs fr-transition-none">
  <ul class="fr-tabs__list" role="tablist" aria-label="">
    <li role="presentation">
      <button id="tabpanel-01" class="fr-tabs__tab fr-icon-checkbox-line fr-tabs__tab--icon-left" tabindex="01" role="tab" aria-selected="true" aria-controls="tabpanel-01-panel">Title1</button>
    </li>
    <li role="presentation">
      <button id="tabpanel-02" class="fr-tabs__tab fr-icon-checkbox-line fr-tabs__tab--icon-left" tabindex="02" role="tab" aria-selected="false" aria-controls="tabpanel-02-panel">Title2</button>
    </li>
  </ul>
  <div id="tabpanel-01-panel" class="fr-tabs__panel" role="tabpanel" aria-labelledby="tabpanel-01" tabindex="01">
    <p>Description1</p>
  </div>
  <div id="tabpanel-02-panel" class="fr-tabs__panel" role="tabpanel" aria-labelledby="tabpanel-02" tabindex="02">
    <p>Description2</p>
  </div>
</div>

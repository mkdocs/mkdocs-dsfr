# Test

<div class="fr-col-md-12 fr-mb-3w">
  <div class="fr-accordions-group">
    <section class="fr-accordion">
      <h3 class="fr-accordion__title">
        <button class="fr-accordion__btn" aria-expanded="false" aria-controls="accordion-01">Title1</button>
      </h3>
      <div class="fr-collapse" id="accordion-01">
        <p>Description1</p>
      </div>
    </section>
    <section class="fr-accordion">
      <h3 class="fr-accordion__title">
        <button class="fr-accordion__btn" aria-expanded="false" aria-controls="accordion-02">Title2</button>
      </h3>
      <div class="fr-collapse" id="accordion-02">
        <p>Description2</p>
      </div>
    </section>
  </div>
</div>

# Test

<div class="fr-alert fr-alert--error">
  <h3 class="fr-alert__title">Error: title</h3>
  <p>Description</p>
</div>

<div class="fr-alert fr-alert--success">
  <h3 class="fr-alert__title">Success: title</h3>
  <p>Description</p>
</div>

<div class="fr-alert fr-alert--info">
  <h3 class="fr-alert__title">Info: title</h3>
  <p>Description</p>
</div>

<div class="fr-alert fr-alert--warning">
  <h3 class="fr-alert__title">Warning: title</h3>
  <p>Description</p>
</div>

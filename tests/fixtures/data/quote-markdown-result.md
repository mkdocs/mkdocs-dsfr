# Test

<figure class="fr-quote fr-quote--column fr-quote--orange-terre-battue">
  <blockquote cite="https://example.com">
    <p>« Lorem [...] elit ut. »</p>
  </blockquote>
  <figcaption>
    <p class="fr-quote__author">Author</p>
    <ul class="fr-quote__source">
      <li>
        <cite>Work Name</cite>
      </li>
      <li>Detail 1</li>
      <li>Detail 2</li>
      <li>Detail 3</li>
      <li>
        <a target="_blank" rel="noopener external" title="Detail 4 - nouvelle fenêtre" href="https://example.com">Detail 4</a>
      </li>
    </ul>
    <div class="fr-quote__image">
      <img src="/assets/images/test.png" class="fr-responsive-img" alt="" />
    </div>
  </figcaption>
</figure>

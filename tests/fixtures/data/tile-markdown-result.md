# Test

<div class="fr-grid-row fr-grid-row--center fr-mb-3w">
  <div class="fr-col fr-col-md-3">
    <div class="fr-tile fr-enlarge-link" id="tile-6609">
      <div class="fr-tile__body">
        <div class="fr-tile__content">
          <h3 class="fr-tile__title">
            <a href="test">Title1</a>
          </h3>
          <p class="fr-tile__desc">Description1</p>
          <p class="fr-tile__detail">Details1</p>
        </div>
      </div>
      <div class="fr-tile__header">
        <div class="fr-tile__pictogram">
          <svg aria-hidden="true" class="fr-artwork" viewBox="0 0 80 80" width="80px" height="80px">
            <use class="fr-artwork-decorative" href="/dsfr_assets/artwork/pictograms/buildings/city-hall.svg#artwork-decorative"></use>
            <use class="fr-artwork-minor" href="/dsfr_assets/artwork/pictograms/buildings/city-hall.svg#artwork-minor"></use>
            <use class="fr-artwork-major" href="/dsfr_assets/artwork/pictograms/buildings/city-hall.svg#artwork-major"></use>
          </svg>
        </div>
      </div>
    </div>
  </div>
  <div class="fr-col fr-col-md-3">
    <div class="fr-tile fr-enlarge-link" id="tile-6609">
      <div class="fr-tile__body">
        <div class="fr-tile__content">
          <h3 class="fr-tile__title">
            <a href="test">Title2</a>
          </h3>
          <p class="fr-tile__desc">Description2</p>
          <p class="fr-tile__detail">Details2</p>
        </div>
      </div>
      <div class="fr-tile__header">
        <div class="fr-tile__pictogram">
          <svg aria-hidden="true" class="fr-artwork" viewBox="0 0 80 80" width="80px" height="80px">
            <use class="fr-artwork-decorative" href="/dsfr_assets/artwork/pictograms/buildings/city-hall.svg#artwork-decorative"></use>
            <use class="fr-artwork-minor" href="/dsfr_assets/artwork/pictograms/buildings/city-hall.svg#artwork-minor"></use>
            <use class="fr-artwork-major" href="/dsfr_assets/artwork/pictograms/buildings/city-hall.svg#artwork-major"></use>
          </svg>
        </div>
      </div>
    </div>
  </div>
</div>

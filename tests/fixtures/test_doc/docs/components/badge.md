# Test Badge

```dsfr-plugin-badge
group:
  - type: "success"
    title: "Label"

  - type: "error"
    title: "Label"

  - type: "info"
    title": "Label"

  - type: "warning"
    title: "Label"

  - type: "new"
    title: "Label"

  - color: "brown-cafe-creme"
    title: "Label"

  - type: "new"
    no_icon: true
    color: "green-menthe"
    sm: true
    title: "Label"
```

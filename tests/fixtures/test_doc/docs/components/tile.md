# Test Tile

```dsfr-plugin-tile
group:
  - title: Title1
    description: Description1
    details: Details1
    link: #
    pictogram: buildings/city-hall
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Title2
    description: Description2
    details: Details2
    link: #
    pictogram: buildings/city-hall
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Title3
    description: Description3
    details: Details3
    link: #
    pictogram: buildings/city-hall
    col_md: 3
    margin_x: 1
    margin_y: 1
```

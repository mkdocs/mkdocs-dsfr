# Test Accordion

```dsfr-plugin-accordion
group:
  - title: "Title1"
    content: |
      Description1:

      - Test
      - Test
  - title: "Title2"
    content: "Description2"
  - title: "Title3"
    content: "Description3"
  - title: "Title4"
    content: "Description4"
  - title: "Title5"
    content: "Description5"
```

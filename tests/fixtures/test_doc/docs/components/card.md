# Test Card

```dsfr-plugin-card
group:
  - title: Title1
    description: Description1
    image: assets/images/test.png
    image_ratio: 1x1
    labels:
      - name: Google
        link: https://google.fr
      - name: Test
        link: https://example.com
    col_md: 2
    margin_x: 1
    margin_y: 1

  - title: Title2
    description: Description2
    link: https://www.systeme-de-design.gouv.fr/
    image: assets/images/test.png
    image_ratio: 1x1
    col_md: 2
    margin_x: 1
    margin_y: 1

  - title: Title3
    description: Description3
    image: assets/images/test.png
    image_ratio: 1x1
    col_md: 2
    margin_x: 1
    margin_y: 1

  - title: Title4
    description: Description4
    image: assets/images/test.png
    image_ratio: 1x1
    col_md: 2
    margin_x: 1
    margin_y: 1

  - title: Title5
    description: Description5
    image: assets/images/test.png
    image_ratio: 1x1
    col_md: 2
    margin_x: 1
    margin_y: 1
```

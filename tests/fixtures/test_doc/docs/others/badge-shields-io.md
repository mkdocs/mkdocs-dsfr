# Test badges shields.io

[![example-1-badge](https://img.shields.io/badge/any%20text-you%20like-blue)](https://example.com)
[![example-2-badge](https://img.shields.io/badge/just%20the%20message-8A2BE2)](https://example.com)
[![example-3-badge](https://img.shields.io/badge/%27for%20the%20badge%27%20style-20B2AA?style=for-the-badge)](https://example.com)
[![example-4-badge](https://img.shields.io/badge/with%20a%20logo-grey?style=for-the-badge&logo=javascript)](https://example.com)

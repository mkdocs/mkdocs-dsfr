# Test icon

:dsfr-icon-check-line:
:dsfr-icon-check-line|green-bourgeon:

:dsfr-icon-close-line:
:dsfr-icon-close-line|red-marianne:

:dsfr-icon-ancient-gate-fill:
:dsfr-icon-ancient-gate-fill|blue-france:

:dsfr-icon-archive-fill:
:dsfr-icon-archive-fill|red-marianne:

:dsfr-icon-chat-2-fill:
:dsfr-icon-chat-2-fill|green-tilleul-verveine:

:dsfr-icon-ball-pen-fill:
:dsfr-icon-ball-pen-fill|green-bourgeon:

:dsfr-icon-bug-fill:
:dsfr-icon-bug-fill|green-emeraude:

:dsfr-icon-bluetooth-fill:
:dsfr-icon-bluetooth-fill|green-menthe:

:dsfr-icon-article-fill:
:dsfr-icon-article-fill|green-archipel:

:dsfr-icon-code-view:
:dsfr-icon-code-view|blue-ecume:

:dsfr-icon-bank-card-fill:
:dsfr-icon-bank-card-fill|blue-cumulus:

:dsfr-icon-capsule-fill:
:dsfr-icon-capsule-fill|purple-glycine:

:dsfr-icon-chrome-fill:
:dsfr-icon-chrome-fill|pink-macaron:

:dsfr-icon-anchor-fill:
:dsfr-icon-anchor-fill|pink-tuile:

:dsfr-icon-align-left:
:dsfr-icon-align-left|yellow-tournesol:

:dsfr-icon-leaf-fill:
:dsfr-icon-leaf-fill|yellow-moutarde:

:dsfr-icon-add-circle-fill:
:dsfr-icon-add-circle-fill|orange-terre-battue:

:dsfr-icon-account-circle-fill:
:dsfr-icon-account-circle-fill|brown-cafe-creme:

:dsfr-icon-cloudy-2-fill:
:dsfr-icon-cloudy-2-fill|brown-caramel:

:dsfr-icon-flashlight-fill:
:dsfr-icon-flashlight-fill|brown-opera:

:dsfr-icon-moon-fill:
:dsfr-icon-moon-fill|beige-gris-galet:

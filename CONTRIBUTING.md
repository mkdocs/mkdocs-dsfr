# Contribuer

## Comment puis-je contribuer ?

### Créer une issue

Si vous souhaitez proposer une nouvelle fonctionnalité, ajouter/modifier la documentation ou signaler un problème, vous pouvez créer une [issue](https://gitlab.mim-libre.fr/mkdocs/mkdocs-dsfr/-/issues). Assurez-vous de choisir le [template d'issue](https://docs.gitlab.com/ee/user/project/description_templates.html#use-the-templates) et de fournir des informations claires et détaillées.

En fonction du type d'issue créée, veuillez ajouter le bon label pour faciliter leur identification :

- `Type | Feature 📦`
- `Type | Bug 👾`
- `Type | Doc 📚`
- `Type | Tech 🛠️`

Il existe aussi d'autres labels qui ne sont pas forcément nécessaires dès la création de l'issue :

- `Complexity`: Estimation de la complexité de l'issue (en taille de t-shirt)
- `Status`: Statut de l'issue (Prête, Bloquée, à redéfinir...)

### Merge Requests

Si vous souhaitez proposer du code pour ajouter une nouvelle fonctionnalité, ajouter/modifier de la documentation ou corriger un problème, vous pouvez créer une merge request  à partir d'une issue ([Documentation](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#from-an-issue)):

```mermaid
%%{init: { 'logLevel': 'debug', 'theme': 'default' } }%%
gitGraph
    commit id: "chore: init project"
    branch 1-issue-name
    checkout 1-issue-name
    commit id: "feat!: add feature"
    commit id: "chore: modification without impact"
    checkout main
    merge 1-issue-name tag: "1.0.0"
    branch 2-other-issue-name
    checkout 2-other-issue-name
    commit id: "feat: add new feature"
    commit id: "fix: bug fix"
    checkout main
    merge 2-other-issue-name tag: "1.1.0"
```

Si vous n'avez pas les droits sur le projet, vous pouvez passer par un [fork](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork).

## Configurer l'environnement de développement

Pour configurer l'environnement de développement de `mkdocs-dsfr`, forkez le dépôt sur votre compte Gitlab.

### Utiliser Gitpod

Vous pouvez commencer à développer le projet dans un environnement en ligne en utilisant [Gitpod](https://gitpod.io/).

Veuillez lire la documentation officielle de [démarrage]((https://www.gitpod.io/docs/introduction/getting-started)) de Gitpod.

### Développement local

Si vous préférez configurer le projet sur votre machine locale, suivez ces étapes :

- Installez Python, Poetry et [Task](https://taskfile.dev/installation/)
- Clonez le dépôt forké
- Exécutez la commande `task init`

## Directives de codage

Veuillez suivre ces standards de codage lors de vos contributions :

- PEP 8 pour le code Python.
- Rédigez des messages de commit clairs et descriptifs (veuillez utiliser les [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)).
- Incluez des tests pour toute nouvelle fonctionnalité ou correction de bug.
- Mettez à jour la documentation dès que possible.
- Assurez-vous que tous les tests passent avant de soumettre une merge request.

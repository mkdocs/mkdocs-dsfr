# MkDocs DSFR

[![gitpod-ready-to-code](https://img.shields.io/badge/gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.mim-libre.fr/mkdocs/mkdocs-dsfr)
![gitlab-pipeline-status](https://img.shields.io/gitlab/pipeline-status/mkdocs%2Fmkdocs-dsfr?gitlab_url=https%3A%2F%2Fgitlab.mim-libre.fr)
[![pypi](https://img.shields.io/pypi/v/mkdocs-dsfr.svg)](https://pypi.org/project/mkdocs-dsfr/)
[![pyversions](https://img.shields.io/pypi/pyversions/mkdocs-dsfr.svg)](https://pypi.python.org/pypi/mkdocs-dsfr)
[![downloads](https://img.shields.io/pypi/dm/mkdocs-dsfr.svg)](https://pypi.org/project/mkdocs-dsfr/)
[![license](https://img.shields.io/pypi/l/mkdocs-dsfr.svg)](https://pypi.python.org/pypi/mkdocs-dsfr)

**MkDocs DSFR** est un portage du
[Système de Design Français](https://www.systeme-de-design.gouv.fr/) (ou DSFR) sous forme de thème [MkDocs](https://www.mkdocs.org/).

## ⚠️ Utilisation interdite en dehors des sites Internet de l'État

>Il est formellement interdit à tout autre acteur d’utiliser le Système de Design de l’État (les administrations territoriales ou tout autre acteur privé) pour des sites web ou des applications. Le Système de Design de l’État représente l’identité numérique de l’État. En cas d’usage à des fins trompeuses ou frauduleuses, l'État se réserve le droit d’entreprendre les actions nécessaires pour y mettre un terme.

👉 Voir README du DSFR [ici](https://github.com/GouvernementFR/dsfr/blob/main/README.md#licence-et-droit-dutilisation).

## Installation

### Installation pip

```sh
pip install mkdocs-dsfr
```

### Configuration

Dans le fichier de configuration `mkdocs.yml`:

```yaml
theme:
  name: dsfr
```

## Contribuer

[CONTRIBUTING.md](CONTRIBUTING.md)

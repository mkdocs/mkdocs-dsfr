# Installation

## Installation du package

Pour installer MkDocs DSFR, vous pouvez le faire via les méthodes suivantes :

**Via pypi** :

```bash
pip install mkdocs-dsfr
```

**Via git** :

```bash
pip install git+https://gitlab.mim-libre.fr/mkdocs/mkdocs-dsfr.git@branch_name
```

Les méthodes précedentes peuvent aussi s'appliquer aux fichiers suivants :

**Via requiments.txt** :

```bash
mkdocs==1.6.1
mkdocs-dsfr==x.y.z

# ou

mkdocs==1.6.1
git+https://gitlab.mim-libre.fr/mkdocs/mkdocs-dsfr.git@main # Remplacer "main" par une branche ou tag de votre choix
```

**Via poetry** :

```toml
[tool.poetry.dependencies]
mkdocs = "^1.6.1"
mkdocs-dsfr = "^x.y.z"

# ou

[tool.poetry.dependencies]
mkdocs = "^1.6.1"
mkdocs-dsfr = { git = "https://gitlab.mim-libre.fr/mkdocs/mkdocs-dsfr.git", branch = "main" } # Remplacer "main" par une branche ou tag de votre choix
```

Pour plus de détails, consultez la documentation [Getting Started with MkDocs](https://www.mkdocs.org/getting-started/).

## Configuration

Variables obligatoires du thème :

|          Nom           |          Exemple de valeur          |             Description             |
| ---------------------- | ----------------------------------- | ----------------------------------- |
| name                   | dsfr                                | Nom du thème (variable MkDocs)      |
| logo_title             | Intitulé<br>Officiel                | Titre du logo (header et footer)    |
| header.service_title   | MkDocs DSFR / Documentation         | Titre principal dans le header      |
| header.service_tagline | Documentation du projet MkDocs DSFR | Description sous le titre principal |

Voici un exemple de configuration pour MkDocs DSFR dans le fichier **mkdocs.yml** :

```yaml
---
# Project information
site_name: Your Site Name
site_url: https://example.com/
site_description: Site description
site_dir: public

# Repository
repo_name: Your Repo Name
repo_url: https://github.com/group-name/your-project

# Theme
theme:
  name: dsfr
  logo_title: Intitulé<br>Officiel
  header:
    service_title: MkDocs DSFR / Documentation
    service_tagline: Documentation du projet MkDocs DSFR

# Plugins
plugins:
  - search
  - dsfr-plugin

# Markdown extensions
markdown_extensions:
  - attr_list
  - pymdownx.emoji:
      emoji_generator: !!python/name:pymdownx.emoji.to_svg
```

# Icones - liste

## buildings

| Icone | Nom |
|-------|-----|
| :dsfr-icon-ancient-gate-fill: | :dsfr&zwj;-icon-ancient-gate-fill: |
| :dsfr-icon-ancient-gate-line: | :dsfr&zwj;-icon-ancient-gate-line: |
| :dsfr-icon-ancient-pavilion-fill: | :dsfr&zwj;-icon-ancient-pavilion-fill: |
| :dsfr-icon-ancient-pavilion-line: | :dsfr&zwj;-icon-ancient-pavilion-line: |
| :dsfr-icon-bank-fill: | :dsfr&zwj;-icon-bank-fill: |
| :dsfr-icon-bank-line: | :dsfr&zwj;-icon-bank-line: |
| :dsfr-icon-building-fill: | :dsfr&zwj;-icon-building-fill: |
| :dsfr-icon-building-line: | :dsfr&zwj;-icon-building-line: |
| :dsfr-icon-community-fill: | :dsfr&zwj;-icon-community-fill: |
| :dsfr-icon-community-line: | :dsfr&zwj;-icon-community-line: |
| :dsfr-icon-government-fill: | :dsfr&zwj;-icon-government-fill: |
| :dsfr-icon-government-line: | :dsfr&zwj;-icon-government-line: |
| :dsfr-icon-home-4-fill: | :dsfr&zwj;-icon-home-4-fill: |
| :dsfr-icon-home-4-line: | :dsfr&zwj;-icon-home-4-line: |
| :dsfr-icon-hospital-fill: | :dsfr&zwj;-icon-hospital-fill: |
| :dsfr-icon-hospital-line: | :dsfr&zwj;-icon-hospital-line: |
| :dsfr-icon-hotel-fill: | :dsfr&zwj;-icon-hotel-fill: |
| :dsfr-icon-hotel-line: | :dsfr&zwj;-icon-hotel-line: |
| :dsfr-icon-store-fill: | :dsfr&zwj;-icon-store-fill: |
| :dsfr-icon-store-line: | :dsfr&zwj;-icon-store-line: |

## business

| Icone | Nom |
|-------|-----|
| :dsfr-icon-archive-fill: | :dsfr&zwj;-icon-archive-fill: |
| :dsfr-icon-archive-line: | :dsfr&zwj;-icon-archive-line: |
| :dsfr-icon-attachment-fill: | :dsfr&zwj;-icon-attachment-fill: |
| :dsfr-icon-attachment-line: | :dsfr&zwj;-icon-attachment-line: |
| :dsfr-icon-award-fill: | :dsfr&zwj;-icon-award-fill: |
| :dsfr-icon-award-line: | :dsfr&zwj;-icon-award-line: |
| :dsfr-icon-bar-chart-box-fill: | :dsfr&zwj;-icon-bar-chart-box-fill: |
| :dsfr-icon-bar-chart-box-line: | :dsfr&zwj;-icon-bar-chart-box-line: |
| :dsfr-icon-bookmark-fill: | :dsfr&zwj;-icon-bookmark-fill: |
| :dsfr-icon-bookmark-line: | :dsfr&zwj;-icon-bookmark-line: |
| :dsfr-icon-briefcase-fill: | :dsfr&zwj;-icon-briefcase-fill: |
| :dsfr-icon-briefcase-line: | :dsfr&zwj;-icon-briefcase-line: |
| :dsfr-icon-calendar-2-fill: | :dsfr&zwj;-icon-calendar-2-fill: |
| :dsfr-icon-calendar-2-line: | :dsfr&zwj;-icon-calendar-2-line: |
| :dsfr-icon-calendar-event-fill: | :dsfr&zwj;-icon-calendar-event-fill: |
| :dsfr-icon-calendar-event-line: | :dsfr&zwj;-icon-calendar-event-line: |
| :dsfr-icon-calendar-fill: | :dsfr&zwj;-icon-calendar-fill: |
| :dsfr-icon-calendar-line: | :dsfr&zwj;-icon-calendar-line: |
| :dsfr-icon-cloud-fill: | :dsfr&zwj;-icon-cloud-fill: |
| :dsfr-icon-cloud-line: | :dsfr&zwj;-icon-cloud-line: |
| :dsfr-icon-copyright-fill: | :dsfr&zwj;-icon-copyright-fill: |
| :dsfr-icon-copyright-line: | :dsfr&zwj;-icon-copyright-line: |
| :dsfr-icon-customer-service-fill: | :dsfr&zwj;-icon-customer-service-fill: |
| :dsfr-icon-customer-service-line: | :dsfr&zwj;-icon-customer-service-line: |
| :dsfr-icon-flag-fill: | :dsfr&zwj;-icon-flag-fill: |
| :dsfr-icon-flag-line: | :dsfr&zwj;-icon-flag-line: |
| :dsfr-icon-global-fill: | :dsfr&zwj;-icon-global-fill: |
| :dsfr-icon-global-line: | :dsfr&zwj;-icon-global-line: |
| :dsfr-icon-line-chart-fill: | :dsfr&zwj;-icon-line-chart-fill: |
| :dsfr-icon-line-chart-line: | :dsfr&zwj;-icon-line-chart-line: |
| :dsfr-icon-links-fill: | :dsfr&zwj;-icon-links-fill: |
| :dsfr-icon-links-line: | :dsfr&zwj;-icon-links-line: |
| :dsfr-icon-mail-fill: | :dsfr&zwj;-icon-mail-fill: |
| :dsfr-icon-mail-line: | :dsfr&zwj;-icon-mail-line: |
| :dsfr-icon-mail-open-fill: | :dsfr&zwj;-icon-mail-open-fill: |
| :dsfr-icon-mail-open-line: | :dsfr&zwj;-icon-mail-open-line: |
| :dsfr-icon-medal-fill: | :dsfr&zwj;-icon-medal-fill: |
| :dsfr-icon-medal-line: | :dsfr&zwj;-icon-medal-line: |
| :dsfr-icon-pie-chart-2-fill: | :dsfr&zwj;-icon-pie-chart-2-fill: |
| :dsfr-icon-pie-chart-2-line: | :dsfr&zwj;-icon-pie-chart-2-line: |
| :dsfr-icon-pie-chart-box-fill: | :dsfr&zwj;-icon-pie-chart-box-fill: |
| :dsfr-icon-pie-chart-box-line: | :dsfr&zwj;-icon-pie-chart-box-line: |
| :dsfr-icon-printer-fill: | :dsfr&zwj;-icon-printer-fill: |
| :dsfr-icon-printer-line: | :dsfr&zwj;-icon-printer-line: |
| :dsfr-icon-profil-fill: | :dsfr&zwj;-icon-profil-fill: |
| :dsfr-icon-profil-line: | :dsfr&zwj;-icon-profil-line: |
| :dsfr-icon-projector-2-fill: | :dsfr&zwj;-icon-projector-2-fill: |
| :dsfr-icon-projector-2-line: | :dsfr&zwj;-icon-projector-2-line: |
| :dsfr-icon-send-plane-fill: | :dsfr&zwj;-icon-send-plane-fill: |
| :dsfr-icon-send-plane-line: | :dsfr&zwj;-icon-send-plane-line: |
| :dsfr-icon-slideshow-fill: | :dsfr&zwj;-icon-slideshow-fill: |
| :dsfr-icon-slideshow-line: | :dsfr&zwj;-icon-slideshow-line: |
| :dsfr-icon-window-fill: | :dsfr&zwj;-icon-window-fill: |
| :dsfr-icon-window-line: | :dsfr&zwj;-icon-window-line: |

## communication

| Icone | Nom |
|-------|-----|
| :dsfr-icon-chat-2-fill: | :dsfr&zwj;-icon-chat-2-fill: |
| :dsfr-icon-chat-2-line: | :dsfr&zwj;-icon-chat-2-line: |
| :dsfr-icon-chat-3-fill: | :dsfr&zwj;-icon-chat-3-fill: |
| :dsfr-icon-chat-3-line: | :dsfr&zwj;-icon-chat-3-line: |
| :dsfr-icon-chat-check-fill: | :dsfr&zwj;-icon-chat-check-fill: |
| :dsfr-icon-chat-check-line: | :dsfr&zwj;-icon-chat-check-line: |
| :dsfr-icon-chat-delete-fill: | :dsfr&zwj;-icon-chat-delete-fill: |
| :dsfr-icon-chat-delete-line: | :dsfr&zwj;-icon-chat-delete-line: |
| :dsfr-icon-chat-poll-fill: | :dsfr&zwj;-icon-chat-poll-fill: |
| :dsfr-icon-chat-poll-line: | :dsfr&zwj;-icon-chat-poll-line: |
| :dsfr-icon-discuss-fill: | :dsfr&zwj;-icon-discuss-fill: |
| :dsfr-icon-discuss-line: | :dsfr&zwj;-icon-discuss-line: |
| :dsfr-icon-feedback-fill: | :dsfr&zwj;-icon-feedback-fill: |
| :dsfr-icon-feedback-line: | :dsfr&zwj;-icon-feedback-line: |
| :dsfr-icon-message-2-fill: | :dsfr&zwj;-icon-message-2-fill: |
| :dsfr-icon-message-2-line: | :dsfr&zwj;-icon-message-2-line: |
| :dsfr-icon-question-answer-fill: | :dsfr&zwj;-icon-question-answer-fill: |
| :dsfr-icon-question-answer-line: | :dsfr&zwj;-icon-question-answer-line: |
| :dsfr-icon-questionnaire-fill: | :dsfr&zwj;-icon-questionnaire-fill: |
| :dsfr-icon-questionnaire-line: | :dsfr&zwj;-icon-questionnaire-line: |
| :dsfr-icon-video-chat-fill: | :dsfr&zwj;-icon-video-chat-fill: |
| :dsfr-icon-video-chat-line: | :dsfr&zwj;-icon-video-chat-line: |

## design

| Icone | Nom |
|-------|-----|
| :dsfr-icon-ball-pen-fill: | :dsfr&zwj;-icon-ball-pen-fill: |
| :dsfr-icon-ball-pen-line: | :dsfr&zwj;-icon-ball-pen-line: |
| :dsfr-icon-brush-3-fill: | :dsfr&zwj;-icon-brush-3-fill: |
| :dsfr-icon-brush-3-line: | :dsfr&zwj;-icon-brush-3-line: |
| :dsfr-icon-brush-fill: | :dsfr&zwj;-icon-brush-fill: |
| :dsfr-icon-brush-line: | :dsfr&zwj;-icon-brush-line: |
| :dsfr-icon-contrast-fill: | :dsfr&zwj;-icon-contrast-fill: |
| :dsfr-icon-contrast-line: | :dsfr&zwj;-icon-contrast-line: |
| :dsfr-icon-crop-fill: | :dsfr&zwj;-icon-crop-fill: |
| :dsfr-icon-crop-line: | :dsfr&zwj;-icon-crop-line: |
| :dsfr-icon-drag-move-2-fill: | :dsfr&zwj;-icon-drag-move-2-fill: |
| :dsfr-icon-drag-move-2-line: | :dsfr&zwj;-icon-drag-move-2-line: |
| :dsfr-icon-drop-fill: | :dsfr&zwj;-icon-drop-fill: |
| :dsfr-icon-drop-line: | :dsfr&zwj;-icon-drop-line: |
| :dsfr-icon-edit-box-fill: | :dsfr&zwj;-icon-edit-box-fill: |
| :dsfr-icon-edit-box-line: | :dsfr&zwj;-icon-edit-box-line: |
| :dsfr-icon-edit-fill: | :dsfr&zwj;-icon-edit-fill: |
| :dsfr-icon-edit-line: | :dsfr&zwj;-icon-edit-line: |
| :dsfr-icon-ink-bottle-fill: | :dsfr&zwj;-icon-ink-bottle-fill: |
| :dsfr-icon-ink-bottle-line: | :dsfr&zwj;-icon-ink-bottle-line: |
| :dsfr-icon-layout-grid-fill: | :dsfr&zwj;-icon-layout-grid-fill: |
| :dsfr-icon-layout-grid-line: | :dsfr&zwj;-icon-layout-grid-line: |
| :dsfr-icon-mark-pen-fill: | :dsfr&zwj;-icon-mark-pen-fill: |
| :dsfr-icon-mark-pen-line: | :dsfr&zwj;-icon-mark-pen-line: |
| :dsfr-icon-paint-brush-fill: | :dsfr&zwj;-icon-paint-brush-fill: |
| :dsfr-icon-paint-brush-line: | :dsfr&zwj;-icon-paint-brush-line: |
| :dsfr-icon-paint-fill: | :dsfr&zwj;-icon-paint-fill: |
| :dsfr-icon-paint-line: | :dsfr&zwj;-icon-paint-line: |
| :dsfr-icon-palette-fill: | :dsfr&zwj;-icon-palette-fill: |
| :dsfr-icon-palette-line: | :dsfr&zwj;-icon-palette-line: |
| :dsfr-icon-pantone-fill: | :dsfr&zwj;-icon-pantone-fill: |
| :dsfr-icon-pantone-line: | :dsfr&zwj;-icon-pantone-line: |
| :dsfr-icon-pen-nib-fill: | :dsfr&zwj;-icon-pen-nib-fill: |
| :dsfr-icon-pen-nib-line: | :dsfr&zwj;-icon-pen-nib-line: |
| :dsfr-icon-pencil-fill: | :dsfr&zwj;-icon-pencil-fill: |
| :dsfr-icon-pencil-line: | :dsfr&zwj;-icon-pencil-line: |
| :dsfr-icon-pencil-ruler-fill: | :dsfr&zwj;-icon-pencil-ruler-fill: |
| :dsfr-icon-pencil-ruler-line: | :dsfr&zwj;-icon-pencil-ruler-line: |
| :dsfr-icon-sip-fill: | :dsfr&zwj;-icon-sip-fill: |
| :dsfr-icon-sip-line: | :dsfr&zwj;-icon-sip-line: |
| :dsfr-icon-table-fill: | :dsfr&zwj;-icon-table-fill: |
| :dsfr-icon-table-line: | :dsfr&zwj;-icon-table-line: |

## development

| Icone | Nom |
|-------|-----|
| :dsfr-icon-bug-fill: | :dsfr&zwj;-icon-bug-fill: |
| :dsfr-icon-bug-line: | :dsfr&zwj;-icon-bug-line: |
| :dsfr-icon-code-box-fill: | :dsfr&zwj;-icon-code-box-fill: |
| :dsfr-icon-code-box-line: | :dsfr&zwj;-icon-code-box-line: |
| :dsfr-icon-code-s-slash-line: | :dsfr&zwj;-icon-code-s-slash-line: |
| :dsfr-icon-cursor-fill: | :dsfr&zwj;-icon-cursor-fill: |
| :dsfr-icon-cursor-line: | :dsfr&zwj;-icon-cursor-line: |
| :dsfr-icon-git-branch-fill: | :dsfr&zwj;-icon-git-branch-fill: |
| :dsfr-icon-git-branch-line: | :dsfr&zwj;-icon-git-branch-line: |
| :dsfr-icon-git-commit-fill: | :dsfr&zwj;-icon-git-commit-fill: |
| :dsfr-icon-git-commit-line: | :dsfr&zwj;-icon-git-commit-line: |
| :dsfr-icon-git-merge-fill: | :dsfr&zwj;-icon-git-merge-fill: |
| :dsfr-icon-git-merge-line: | :dsfr&zwj;-icon-git-merge-line: |
| :dsfr-icon-git-pull-request-fill: | :dsfr&zwj;-icon-git-pull-request-fill: |
| :dsfr-icon-git-pull-request-line: | :dsfr&zwj;-icon-git-pull-request-line: |
| :dsfr-icon-git-repository-commits-fill: | :dsfr&zwj;-icon-git-repository-commits-fill: |
| :dsfr-icon-git-repository-commits-line: | :dsfr&zwj;-icon-git-repository-commits-line: |
| :dsfr-icon-git-repository-fill: | :dsfr&zwj;-icon-git-repository-fill: |
| :dsfr-icon-git-repository-line: | :dsfr&zwj;-icon-git-repository-line: |
| :dsfr-icon-git-repository-private-fill: | :dsfr&zwj;-icon-git-repository-private-fill: |
| :dsfr-icon-git-repository-private-line: | :dsfr&zwj;-icon-git-repository-private-line: |
| :dsfr-icon-terminal-box-fill: | :dsfr&zwj;-icon-terminal-box-fill: |
| :dsfr-icon-terminal-box-line: | :dsfr&zwj;-icon-terminal-box-line: |
| :dsfr-icon-terminal-line: | :dsfr&zwj;-icon-terminal-line: |
| :dsfr-icon-terminal-window-fill: | :dsfr&zwj;-icon-terminal-window-fill: |
| :dsfr-icon-terminal-window-line: | :dsfr&zwj;-icon-terminal-window-line: |

## device

| Icone | Nom |
|-------|-----|
| :dsfr-icon-bluetooth-fill: | :dsfr&zwj;-icon-bluetooth-fill: |
| :dsfr-icon-bluetooth-line: | :dsfr&zwj;-icon-bluetooth-line: |
| :dsfr-icon-computer-fill: | :dsfr&zwj;-icon-computer-fill: |
| :dsfr-icon-computer-line: | :dsfr&zwj;-icon-computer-line: |
| :dsfr-icon-dashboard-3-fill: | :dsfr&zwj;-icon-dashboard-3-fill: |
| :dsfr-icon-dashboard-3-line: | :dsfr&zwj;-icon-dashboard-3-line: |
| :dsfr-icon-database-fill: | :dsfr&zwj;-icon-database-fill: |
| :dsfr-icon-database-line: | :dsfr&zwj;-icon-database-line: |
| :dsfr-icon-device-fill: | :dsfr&zwj;-icon-device-fill: |
| :dsfr-icon-device-line: | :dsfr&zwj;-icon-device-line: |
| :dsfr-icon-hard-drive-2-fill: | :dsfr&zwj;-icon-hard-drive-2-fill: |
| :dsfr-icon-hard-drive-2-line: | :dsfr&zwj;-icon-hard-drive-2-line: |
| :dsfr-icon-mac-fill: | :dsfr&zwj;-icon-mac-fill: |
| :dsfr-icon-mac-line: | :dsfr&zwj;-icon-mac-line: |
| :dsfr-icon-phone-fill: | :dsfr&zwj;-icon-phone-fill: |
| :dsfr-icon-phone-line: | :dsfr&zwj;-icon-phone-line: |
| :dsfr-icon-qr-code-fill: | :dsfr&zwj;-icon-qr-code-fill: |
| :dsfr-icon-qr-code-line: | :dsfr&zwj;-icon-qr-code-line: |
| :dsfr-icon-rss-fill: | :dsfr&zwj;-icon-rss-fill: |
| :dsfr-icon-rss-line: | :dsfr&zwj;-icon-rss-line: |
| :dsfr-icon-save-3-fill: | :dsfr&zwj;-icon-save-3-fill: |
| :dsfr-icon-save-3-line: | :dsfr&zwj;-icon-save-3-line: |
| :dsfr-icon-save-fill: | :dsfr&zwj;-icon-save-fill: |
| :dsfr-icon-save-line: | :dsfr&zwj;-icon-save-line: |
| :dsfr-icon-server-fill: | :dsfr&zwj;-icon-server-fill: |
| :dsfr-icon-server-line: | :dsfr&zwj;-icon-server-line: |
| :dsfr-icon-smartphone-fill: | :dsfr&zwj;-icon-smartphone-fill: |
| :dsfr-icon-smartphone-line: | :dsfr&zwj;-icon-smartphone-line: |
| :dsfr-icon-tablet-fill: | :dsfr&zwj;-icon-tablet-fill: |
| :dsfr-icon-tablet-line: | :dsfr&zwj;-icon-tablet-line: |
| :dsfr-icon-tv-fill: | :dsfr&zwj;-icon-tv-fill: |
| :dsfr-icon-tv-line: | :dsfr&zwj;-icon-tv-line: |
| :dsfr-icon-wifi-fill: | :dsfr&zwj;-icon-wifi-fill: |
| :dsfr-icon-wifi-line: | :dsfr&zwj;-icon-wifi-line: |

## document

| Icone | Nom |
|-------|-----|
| :dsfr-icon-article-fill: | :dsfr&zwj;-icon-article-fill: |
| :dsfr-icon-article-line: | :dsfr&zwj;-icon-article-line: |
| :dsfr-icon-book-2-fill: | :dsfr&zwj;-icon-book-2-fill: |
| :dsfr-icon-book-2-line: | :dsfr&zwj;-icon-book-2-line: |
| :dsfr-icon-booklet-fill: | :dsfr&zwj;-icon-booklet-fill: |
| :dsfr-icon-booklet-line: | :dsfr&zwj;-icon-booklet-line: |
| :dsfr-icon-clipboard-fill: | :dsfr&zwj;-icon-clipboard-fill: |
| :dsfr-icon-clipboard-line: | :dsfr&zwj;-icon-clipboard-line: |
| :dsfr-icon-draft-fill: | :dsfr&zwj;-icon-draft-fill: |
| :dsfr-icon-draft-line: | :dsfr&zwj;-icon-draft-line: |
| :dsfr-icon-file-add-fill: | :dsfr&zwj;-icon-file-add-fill: |
| :dsfr-icon-file-add-line: | :dsfr&zwj;-icon-file-add-line: |
| :dsfr-icon-file-download-fill: | :dsfr&zwj;-icon-file-download-fill: |
| :dsfr-icon-file-download-line: | :dsfr&zwj;-icon-file-download-line: |
| :dsfr-icon-file-fill: | :dsfr&zwj;-icon-file-fill: |
| :dsfr-icon-file-line: | :dsfr&zwj;-icon-file-line: |
| :dsfr-icon-file-pdf-fill: | :dsfr&zwj;-icon-file-pdf-fill: |
| :dsfr-icon-file-pdf-line: | :dsfr&zwj;-icon-file-pdf-line: |
| :dsfr-icon-file-text-fill: | :dsfr&zwj;-icon-file-text-fill: |
| :dsfr-icon-file-text-line: | :dsfr&zwj;-icon-file-text-line: |
| :dsfr-icon-folder-2-fill: | :dsfr&zwj;-icon-folder-2-fill: |
| :dsfr-icon-folder-2-line: | :dsfr&zwj;-icon-folder-2-line: |
| :dsfr-icon-newspaper-fill: | :dsfr&zwj;-icon-newspaper-fill: |
| :dsfr-icon-newspaper-line: | :dsfr&zwj;-icon-newspaper-line: |
| :dsfr-icon-survey-fill: | :dsfr&zwj;-icon-survey-fill: |
| :dsfr-icon-survey-line: | :dsfr&zwj;-icon-survey-line: |
| :dsfr-icon-todo-fill: | :dsfr&zwj;-icon-todo-fill: |
| :dsfr-icon-todo-line: | :dsfr&zwj;-icon-todo-line: |

## editor

| Icone | Nom |
|-------|-----|
| :dsfr-icon-code-view: | :dsfr&zwj;-icon-code-view: |
| :dsfr-icon-font-size: | :dsfr&zwj;-icon-font-size: |
| :dsfr-icon-fr--bold: | :dsfr&zwj;-icon-fr--bold: |
| :dsfr-icon-fr--highlight: | :dsfr&zwj;-icon-fr--highlight: |
| :dsfr-icon-fr--quote-fill: | :dsfr&zwj;-icon-fr--quote-fill: |
| :dsfr-icon-fr--quote-line: | :dsfr&zwj;-icon-fr--quote-line: |
| :dsfr-icon-h-1: | :dsfr&zwj;-icon-h-1: |
| :dsfr-icon-h-2: | :dsfr&zwj;-icon-h-2: |
| :dsfr-icon-h-3: | :dsfr&zwj;-icon-h-3: |
| :dsfr-icon-h-4: | :dsfr&zwj;-icon-h-4: |
| :dsfr-icon-h-5: | :dsfr&zwj;-icon-h-5: |
| :dsfr-icon-h-6: | :dsfr&zwj;-icon-h-6: |
| :dsfr-icon-hashtag: | :dsfr&zwj;-icon-hashtag: |
| :dsfr-icon-italic: | :dsfr&zwj;-icon-italic: |
| :dsfr-icon-link-unlink: | :dsfr&zwj;-icon-link-unlink: |
| :dsfr-icon-link: | :dsfr&zwj;-icon-link: |
| :dsfr-icon-list-ordered: | :dsfr&zwj;-icon-list-ordered: |
| :dsfr-icon-list-unordered: | :dsfr&zwj;-icon-list-unordered: |
| :dsfr-icon-question-mark: | :dsfr&zwj;-icon-question-mark: |
| :dsfr-icon-separator: | :dsfr&zwj;-icon-separator: |
| :dsfr-icon-space: | :dsfr&zwj;-icon-space: |
| :dsfr-icon-subscript: | :dsfr&zwj;-icon-subscript: |
| :dsfr-icon-superscript: | :dsfr&zwj;-icon-superscript: |
| :dsfr-icon-table-2: | :dsfr&zwj;-icon-table-2: |
| :dsfr-icon-translate-2: | :dsfr&zwj;-icon-translate-2: |

## finance

| Icone | Nom |
|-------|-----|
| :dsfr-icon-bank-card-fill: | :dsfr&zwj;-icon-bank-card-fill: |
| :dsfr-icon-bank-card-line: | :dsfr&zwj;-icon-bank-card-line: |
| :dsfr-icon-coin-fill: | :dsfr&zwj;-icon-coin-fill: |
| :dsfr-icon-gift-fill: | :dsfr&zwj;-icon-gift-fill: |
| :dsfr-icon-gift-line: | :dsfr&zwj;-icon-gift-line: |
| :dsfr-icon-money-euro-box-fill: | :dsfr&zwj;-icon-money-euro-box-fill: |
| :dsfr-icon-money-euro-box-line: | :dsfr&zwj;-icon-money-euro-box-line: |
| :dsfr-icon-money-euro-circle-fill: | :dsfr&zwj;-icon-money-euro-circle-fill: |
| :dsfr-icon-money-euro-circle-line: | :dsfr&zwj;-icon-money-euro-circle-line: |
| :dsfr-icon-secure-payment-fill: | :dsfr&zwj;-icon-secure-payment-fill: |
| :dsfr-icon-secure-payment-line: | :dsfr&zwj;-icon-secure-payment-line: |
| :dsfr-icon-shopping-bag-fill: | :dsfr&zwj;-icon-shopping-bag-fill: |
| :dsfr-icon-shopping-bag-line: | :dsfr&zwj;-icon-shopping-bag-line: |
| :dsfr-icon-shopping-cart-2-fill: | :dsfr&zwj;-icon-shopping-cart-2-fill: |
| :dsfr-icon-shopping-cart-2-line: | :dsfr&zwj;-icon-shopping-cart-2-line: |
| :dsfr-icon-trophy-fill: | :dsfr&zwj;-icon-trophy-fill: |
| :dsfr-icon-trophy-line: | :dsfr&zwj;-icon-trophy-line: |

## health

| Icone | Nom |
|-------|-----|
| :dsfr-icon-capsule-fill: | :dsfr&zwj;-icon-capsule-fill: |
| :dsfr-icon-capsule-line: | :dsfr&zwj;-icon-capsule-line: |
| :dsfr-icon-dislike-fill: | :dsfr&zwj;-icon-dislike-fill: |
| :dsfr-icon-dislike-line: | :dsfr&zwj;-icon-dislike-line: |
| :dsfr-icon-dossier-fill: | :dsfr&zwj;-icon-dossier-fill: |
| :dsfr-icon-dossier-line: | :dsfr&zwj;-icon-dossier-line: |
| :dsfr-icon-first-aid-kit-fill: | :dsfr&zwj;-icon-first-aid-kit-fill: |
| :dsfr-icon-first-aid-kit-line: | :dsfr&zwj;-icon-first-aid-kit-line: |
| :dsfr-icon-hand-sanitizer-fill: | :dsfr&zwj;-icon-hand-sanitizer-fill: |
| :dsfr-icon-hand-sanitizer-line: | :dsfr&zwj;-icon-hand-sanitizer-line: |
| :dsfr-icon-health-book-fill: | :dsfr&zwj;-icon-health-book-fill: |
| :dsfr-icon-health-book-line: | :dsfr&zwj;-icon-health-book-line: |
| :dsfr-icon-heart-fill: | :dsfr&zwj;-icon-heart-fill: |
| :dsfr-icon-heart-line: | :dsfr&zwj;-icon-heart-line: |
| :dsfr-icon-heart-pulse-fill: | :dsfr&zwj;-icon-heart-pulse-fill: |
| :dsfr-icon-heart-pulse-line: | :dsfr&zwj;-icon-heart-pulse-line: |
| :dsfr-icon-lungs-fill: | :dsfr&zwj;-icon-lungs-fill: |
| :dsfr-icon-lungs-line: | :dsfr&zwj;-icon-lungs-line: |
| :dsfr-icon-medicine-bottle-fill: | :dsfr&zwj;-icon-medicine-bottle-fill: |
| :dsfr-icon-medicine-bottle-line: | :dsfr&zwj;-icon-medicine-bottle-line: |
| :dsfr-icon-mental-health-fill: | :dsfr&zwj;-icon-mental-health-fill: |
| :dsfr-icon-mental-health-line: | :dsfr&zwj;-icon-mental-health-line: |
| :dsfr-icon-microscope-fill: | :dsfr&zwj;-icon-microscope-fill: |
| :dsfr-icon-microscope-line: | :dsfr&zwj;-icon-microscope-line: |
| :dsfr-icon-psychotherapy-fill: | :dsfr&zwj;-icon-psychotherapy-fill: |
| :dsfr-icon-psychotherapy-line: | :dsfr&zwj;-icon-psychotherapy-line: |
| :dsfr-icon-pulse-line: | :dsfr&zwj;-icon-pulse-line: |
| :dsfr-icon-stethoscope-fill: | :dsfr&zwj;-icon-stethoscope-fill: |
| :dsfr-icon-stethoscope-line: | :dsfr&zwj;-icon-stethoscope-line: |
| :dsfr-icon-surgical-mask-fill: | :dsfr&zwj;-icon-surgical-mask-fill: |
| :dsfr-icon-surgical-mask-line: | :dsfr&zwj;-icon-surgical-mask-line: |
| :dsfr-icon-syringe-fill: | :dsfr&zwj;-icon-syringe-fill: |
| :dsfr-icon-syringe-line: | :dsfr&zwj;-icon-syringe-line: |
| :dsfr-icon-test-tube-fill: | :dsfr&zwj;-icon-test-tube-fill: |
| :dsfr-icon-test-tube-line: | :dsfr&zwj;-icon-test-tube-line: |
| :dsfr-icon-thermometer-fill: | :dsfr&zwj;-icon-thermometer-fill: |
| :dsfr-icon-thermometer-line: | :dsfr&zwj;-icon-thermometer-line: |
| :dsfr-icon-virus-fill: | :dsfr&zwj;-icon-virus-fill: |
| :dsfr-icon-virus-line: | :dsfr&zwj;-icon-virus-line: |

## logo

| Icone | Nom |
|-------|-----|
| :dsfr-icon-chrome-fill: | :dsfr&zwj;-icon-chrome-fill: |
| :dsfr-icon-chrome-line: | :dsfr&zwj;-icon-chrome-line: |
| :dsfr-icon-edge-fill: | :dsfr&zwj;-icon-edge-fill: |
| :dsfr-icon-edge-line: | :dsfr&zwj;-icon-edge-line: |
| :dsfr-icon-facebook-circle-fill: | :dsfr&zwj;-icon-facebook-circle-fill: |
| :dsfr-icon-facebook-circle-line: | :dsfr&zwj;-icon-facebook-circle-line: |
| :dsfr-icon-firefox-fill: | :dsfr&zwj;-icon-firefox-fill: |
| :dsfr-icon-firefox-line: | :dsfr&zwj;-icon-firefox-line: |
| :dsfr-icon-fr--dailymotion-fill: | :dsfr&zwj;-icon-fr--dailymotion-fill: |
| :dsfr-icon-fr--dailymotion-line: | :dsfr&zwj;-icon-fr--dailymotion-line: |
| :dsfr-icon-fr--tiktok-fill: | :dsfr&zwj;-icon-fr--tiktok-fill: |
| :dsfr-icon-fr--tiktok-line: | :dsfr&zwj;-icon-fr--tiktok-line: |
| :dsfr-icon-github-fill: | :dsfr&zwj;-icon-github-fill: |
| :dsfr-icon-github-line: | :dsfr&zwj;-icon-github-line: |
| :dsfr-icon-google-fill: | :dsfr&zwj;-icon-google-fill: |
| :dsfr-icon-google-line: | :dsfr&zwj;-icon-google-line: |
| :dsfr-icon-ie-fill: | :dsfr&zwj;-icon-ie-fill: |
| :dsfr-icon-ie-line: | :dsfr&zwj;-icon-ie-line: |
| :dsfr-icon-instagram-fill: | :dsfr&zwj;-icon-instagram-fill: |
| :dsfr-icon-instagram-line: | :dsfr&zwj;-icon-instagram-line: |
| :dsfr-icon-linkedin-box-fill: | :dsfr&zwj;-icon-linkedin-box-fill: |
| :dsfr-icon-linkedin-box-line: | :dsfr&zwj;-icon-linkedin-box-line: |
| :dsfr-icon-mastodon-fill: | :dsfr&zwj;-icon-mastodon-fill: |
| :dsfr-icon-mastodon-line: | :dsfr&zwj;-icon-mastodon-line: |
| :dsfr-icon-npmjs-fill: | :dsfr&zwj;-icon-npmjs-fill: |
| :dsfr-icon-npmjs-line: | :dsfr&zwj;-icon-npmjs-line: |
| :dsfr-icon-remixicon-fill: | :dsfr&zwj;-icon-remixicon-fill: |
| :dsfr-icon-remixicon-line: | :dsfr&zwj;-icon-remixicon-line: |
| :dsfr-icon-safari-fill: | :dsfr&zwj;-icon-safari-fill: |
| :dsfr-icon-safari-line: | :dsfr&zwj;-icon-safari-line: |
| :dsfr-icon-slack-fill: | :dsfr&zwj;-icon-slack-fill: |
| :dsfr-icon-slack-line: | :dsfr&zwj;-icon-slack-line: |
| :dsfr-icon-snapchat-fill: | :dsfr&zwj;-icon-snapchat-fill: |
| :dsfr-icon-snapchat-line: | :dsfr&zwj;-icon-snapchat-line: |
| :dsfr-icon-telegram-fill: | :dsfr&zwj;-icon-telegram-fill: |
| :dsfr-icon-telegram-line: | :dsfr&zwj;-icon-telegram-line: |
| :dsfr-icon-threads-fill: | :dsfr&zwj;-icon-threads-fill: |
| :dsfr-icon-threads-line: | :dsfr&zwj;-icon-threads-line: |
| :dsfr-icon-twitch-fill: | :dsfr&zwj;-icon-twitch-fill: |
| :dsfr-icon-twitch-line: | :dsfr&zwj;-icon-twitch-line: |
| :dsfr-icon-twitter-fill: | :dsfr&zwj;-icon-twitter-fill: |
| :dsfr-icon-twitter-line: | :dsfr&zwj;-icon-twitter-line: |
| :dsfr-icon-twitter-x-fill: | :dsfr&zwj;-icon-twitter-x-fill: |
| :dsfr-icon-twitter-x-line: | :dsfr&zwj;-icon-twitter-x-line: |
| :dsfr-icon-vimeo-fill: | :dsfr&zwj;-icon-vimeo-fill: |
| :dsfr-icon-vimeo-line: | :dsfr&zwj;-icon-vimeo-line: |
| :dsfr-icon-vuejs-fill: | :dsfr&zwj;-icon-vuejs-fill: |
| :dsfr-icon-vuejs-line: | :dsfr&zwj;-icon-vuejs-line: |
| :dsfr-icon-youtube-fill: | :dsfr&zwj;-icon-youtube-fill: |
| :dsfr-icon-youtube-line: | :dsfr&zwj;-icon-youtube-line: |

## map

| Icone | Nom |
|-------|-----|
| :dsfr-icon-anchor-fill: | :dsfr&zwj;-icon-anchor-fill: |
| :dsfr-icon-anchor-line: | :dsfr&zwj;-icon-anchor-line: |
| :dsfr-icon-bike-fill: | :dsfr&zwj;-icon-bike-fill: |
| :dsfr-icon-bike-line: | :dsfr&zwj;-icon-bike-line: |
| :dsfr-icon-bus-fill: | :dsfr&zwj;-icon-bus-fill: |
| :dsfr-icon-bus-line: | :dsfr&zwj;-icon-bus-line: |
| :dsfr-icon-car-fill: | :dsfr&zwj;-icon-car-fill: |
| :dsfr-icon-car-line: | :dsfr&zwj;-icon-car-line: |
| :dsfr-icon-caravan-fill: | :dsfr&zwj;-icon-caravan-fill: |
| :dsfr-icon-caravan-line: | :dsfr&zwj;-icon-caravan-line: |
| :dsfr-icon-charging-pile-2-fill: | :dsfr&zwj;-icon-charging-pile-2-fill: |
| :dsfr-icon-charging-pile-2-line: | :dsfr&zwj;-icon-charging-pile-2-line: |
| :dsfr-icon-compass-3-fill: | :dsfr&zwj;-icon-compass-3-fill: |
| :dsfr-icon-compass-3-line: | :dsfr&zwj;-icon-compass-3-line: |
| :dsfr-icon-cup-fill: | :dsfr&zwj;-icon-cup-fill: |
| :dsfr-icon-cup-line: | :dsfr&zwj;-icon-cup-line: |
| :dsfr-icon-earth-fill: | :dsfr&zwj;-icon-earth-fill: |
| :dsfr-icon-earth-line: | :dsfr&zwj;-icon-earth-line: |
| :dsfr-icon-france-fill: | :dsfr&zwj;-icon-france-fill: |
| :dsfr-icon-france-line: | :dsfr&zwj;-icon-france-line: |
| :dsfr-icon-gas-station-fill: | :dsfr&zwj;-icon-gas-station-fill: |
| :dsfr-icon-gas-station-line: | :dsfr&zwj;-icon-gas-station-line: |
| :dsfr-icon-goblet-fill: | :dsfr&zwj;-icon-goblet-fill: |
| :dsfr-icon-goblet-line: | :dsfr&zwj;-icon-goblet-line: |
| :dsfr-icon-map-pin-2-fill: | :dsfr&zwj;-icon-map-pin-2-fill: |
| :dsfr-icon-map-pin-2-line: | :dsfr&zwj;-icon-map-pin-2-line: |
| :dsfr-icon-map-pin-user-fill: | :dsfr&zwj;-icon-map-pin-user-fill: |
| :dsfr-icon-map-pin-user-line: | :dsfr&zwj;-icon-map-pin-user-line: |
| :dsfr-icon-motorbike-fill: | :dsfr&zwj;-icon-motorbike-fill: |
| :dsfr-icon-motorbike-line: | :dsfr&zwj;-icon-motorbike-line: |
| :dsfr-icon-passport-fill: | :dsfr&zwj;-icon-passport-fill: |
| :dsfr-icon-passport-line: | :dsfr&zwj;-icon-passport-line: |
| :dsfr-icon-restaurant-fill: | :dsfr&zwj;-icon-restaurant-fill: |
| :dsfr-icon-restaurant-line: | :dsfr&zwj;-icon-restaurant-line: |
| :dsfr-icon-road-map-fill: | :dsfr&zwj;-icon-road-map-fill: |
| :dsfr-icon-road-map-line: | :dsfr&zwj;-icon-road-map-line: |
| :dsfr-icon-sailboat-fill: | :dsfr&zwj;-icon-sailboat-fill: |
| :dsfr-icon-sailboat-line: | :dsfr&zwj;-icon-sailboat-line: |
| :dsfr-icon-ship-2-fill: | :dsfr&zwj;-icon-ship-2-fill: |
| :dsfr-icon-ship-2-line: | :dsfr&zwj;-icon-ship-2-line: |
| :dsfr-icon-signal-tower-fill: | :dsfr&zwj;-icon-signal-tower-fill: |
| :dsfr-icon-signal-tower-line: | :dsfr&zwj;-icon-signal-tower-line: |
| :dsfr-icon-suitcase-2-fill: | :dsfr&zwj;-icon-suitcase-2-fill: |
| :dsfr-icon-suitcase-2-line: | :dsfr&zwj;-icon-suitcase-2-line: |
| :dsfr-icon-taxi-fill: | :dsfr&zwj;-icon-taxi-fill: |
| :dsfr-icon-taxi-line: | :dsfr&zwj;-icon-taxi-line: |
| :dsfr-icon-train-fill: | :dsfr&zwj;-icon-train-fill: |
| :dsfr-icon-train-line: | :dsfr&zwj;-icon-train-line: |

## media

| Icone | Nom |
|-------|-----|
| :dsfr-icon-align-left: | :dsfr&zwj;-icon-align-left: |
| :dsfr-icon-camera-fill: | :dsfr&zwj;-icon-camera-fill: |
| :dsfr-icon-camera-line: | :dsfr&zwj;-icon-camera-line: |
| :dsfr-icon-clapperboard-fill: | :dsfr&zwj;-icon-clapperboard-fill: |
| :dsfr-icon-clapperboard-line: | :dsfr&zwj;-icon-clapperboard-line: |
| :dsfr-icon-equalizer-fill: | :dsfr&zwj;-icon-equalizer-fill: |
| :dsfr-icon-equalizer-line: | :dsfr&zwj;-icon-equalizer-line: |
| :dsfr-icon-film-fill: | :dsfr&zwj;-icon-film-fill: |
| :dsfr-icon-film-line: | :dsfr&zwj;-icon-film-line: |
| :dsfr-icon-fullscreen-line: | :dsfr&zwj;-icon-fullscreen-line: |
| :dsfr-icon-gallery-fill: | :dsfr&zwj;-icon-gallery-fill: |
| :dsfr-icon-gallery-line: | :dsfr&zwj;-icon-gallery-line: |
| :dsfr-icon-headphone-fill: | :dsfr&zwj;-icon-headphone-fill: |
| :dsfr-icon-headphone-line: | :dsfr&zwj;-icon-headphone-line: |
| :dsfr-icon-image-add-fill: | :dsfr&zwj;-icon-image-add-fill: |
| :dsfr-icon-image-add-line: | :dsfr&zwj;-icon-image-add-line: |
| :dsfr-icon-image-edit-fill: | :dsfr&zwj;-icon-image-edit-fill: |
| :dsfr-icon-image-edit-line: | :dsfr&zwj;-icon-image-edit-line: |
| :dsfr-icon-image-fill: | :dsfr&zwj;-icon-image-fill: |
| :dsfr-icon-image-line: | :dsfr&zwj;-icon-image-line: |
| :dsfr-icon-live-fill: | :dsfr&zwj;-icon-live-fill: |
| :dsfr-icon-live-line: | :dsfr&zwj;-icon-live-line: |
| :dsfr-icon-mic-fill: | :dsfr&zwj;-icon-mic-fill: |
| :dsfr-icon-mic-line: | :dsfr&zwj;-icon-mic-line: |
| :dsfr-icon-music-2-fill: | :dsfr&zwj;-icon-music-2-fill: |
| :dsfr-icon-music-2-line: | :dsfr&zwj;-icon-music-2-line: |
| :dsfr-icon-notification-3-fill: | :dsfr&zwj;-icon-notification-3-fill: |
| :dsfr-icon-notification-3-line: | :dsfr&zwj;-icon-notification-3-line: |
| :dsfr-icon-pause-circle-fill: | :dsfr&zwj;-icon-pause-circle-fill: |
| :dsfr-icon-pause-circle-line: | :dsfr&zwj;-icon-pause-circle-line: |
| :dsfr-icon-play-circle-fill: | :dsfr&zwj;-icon-play-circle-fill: |
| :dsfr-icon-play-circle-line: | :dsfr&zwj;-icon-play-circle-line: |
| :dsfr-icon-stop-circle-fill: | :dsfr&zwj;-icon-stop-circle-fill: |
| :dsfr-icon-stop-circle-line: | :dsfr&zwj;-icon-stop-circle-line: |
| :dsfr-icon-volume-down-fill: | :dsfr&zwj;-icon-volume-down-fill: |
| :dsfr-icon-volume-down-line: | :dsfr&zwj;-icon-volume-down-line: |
| :dsfr-icon-volume-mute-fill: | :dsfr&zwj;-icon-volume-mute-fill: |
| :dsfr-icon-volume-mute-line: | :dsfr&zwj;-icon-volume-mute-line: |
| :dsfr-icon-volume-up-fill: | :dsfr&zwj;-icon-volume-up-fill: |
| :dsfr-icon-volume-up-line: | :dsfr&zwj;-icon-volume-up-line: |

## others

| Icone | Nom |
|-------|-----|
| :dsfr-icon-leaf-fill: | :dsfr&zwj;-icon-leaf-fill: |
| :dsfr-icon-leaf-line: | :dsfr&zwj;-icon-leaf-line: |
| :dsfr-icon-lightbulb-fill: | :dsfr&zwj;-icon-lightbulb-fill: |
| :dsfr-icon-lightbulb-line: | :dsfr&zwj;-icon-lightbulb-line: |
| :dsfr-icon-plant-fill: | :dsfr&zwj;-icon-plant-fill: |
| :dsfr-icon-plant-line: | :dsfr&zwj;-icon-plant-line: |
| :dsfr-icon-recycle-fill: | :dsfr&zwj;-icon-recycle-fill: |
| :dsfr-icon-recycle-line: | :dsfr&zwj;-icon-recycle-line: |
| :dsfr-icon-scales-3-fill: | :dsfr&zwj;-icon-scales-3-fill: |
| :dsfr-icon-scales-3-line: | :dsfr&zwj;-icon-scales-3-line: |
| :dsfr-icon-seedling-fill: | :dsfr&zwj;-icon-seedling-fill: |
| :dsfr-icon-seedling-line: | :dsfr&zwj;-icon-seedling-line: |
| :dsfr-icon-umbrella-fill: | :dsfr&zwj;-icon-umbrella-fill: |
| :dsfr-icon-umbrella-line: | :dsfr&zwj;-icon-umbrella-line: |

## system

| Icone | Nom |
|-------|-----|
| :dsfr-icon-add-circle-fill: | :dsfr&zwj;-icon-add-circle-fill: |
| :dsfr-icon-add-circle-line: | :dsfr&zwj;-icon-add-circle-line: |
| :dsfr-icon-add-line: | :dsfr&zwj;-icon-add-line: |
| :dsfr-icon-alarm-warning-fill: | :dsfr&zwj;-icon-alarm-warning-fill: |
| :dsfr-icon-alarm-warning-line: | :dsfr&zwj;-icon-alarm-warning-line: |
| :dsfr-icon-alert-fill: | :dsfr&zwj;-icon-alert-fill: |
| :dsfr-icon-alert-line: | :dsfr&zwj;-icon-alert-line: |
| :dsfr-icon-arrow-down-fill: | :dsfr&zwj;-icon-arrow-down-fill: |
| :dsfr-icon-arrow-down-line: | :dsfr&zwj;-icon-arrow-down-line: |
| :dsfr-icon-arrow-down-s-fill: | :dsfr&zwj;-icon-arrow-down-s-fill: |
| :dsfr-icon-arrow-down-s-line: | :dsfr&zwj;-icon-arrow-down-s-line: |
| :dsfr-icon-arrow-go-back-fill: | :dsfr&zwj;-icon-arrow-go-back-fill: |
| :dsfr-icon-arrow-go-back-line: | :dsfr&zwj;-icon-arrow-go-back-line: |
| :dsfr-icon-arrow-go-forward-fill: | :dsfr&zwj;-icon-arrow-go-forward-fill: |
| :dsfr-icon-arrow-go-forward-line: | :dsfr&zwj;-icon-arrow-go-forward-line: |
| :dsfr-icon-arrow-left-fill: | :dsfr&zwj;-icon-arrow-left-fill: |
| :dsfr-icon-arrow-left-line: | :dsfr&zwj;-icon-arrow-left-line: |
| :dsfr-icon-arrow-left-s-fill: | :dsfr&zwj;-icon-arrow-left-s-fill: |
| :dsfr-icon-arrow-left-s-line: | :dsfr&zwj;-icon-arrow-left-s-line: |
| :dsfr-icon-arrow-right-fill: | :dsfr&zwj;-icon-arrow-right-fill: |
| :dsfr-icon-arrow-right-line: | :dsfr&zwj;-icon-arrow-right-line: |
| :dsfr-icon-arrow-right-s-fill: | :dsfr&zwj;-icon-arrow-right-s-fill: |
| :dsfr-icon-arrow-right-s-line: | :dsfr&zwj;-icon-arrow-right-s-line: |
| :dsfr-icon-arrow-right-up-line: | :dsfr&zwj;-icon-arrow-right-up-line: |
| :dsfr-icon-arrow-up-fill: | :dsfr&zwj;-icon-arrow-up-fill: |
| :dsfr-icon-arrow-up-line: | :dsfr&zwj;-icon-arrow-up-line: |
| :dsfr-icon-arrow-up-s-fill: | :dsfr&zwj;-icon-arrow-up-s-fill: |
| :dsfr-icon-arrow-up-s-line: | :dsfr&zwj;-icon-arrow-up-s-line: |
| :dsfr-icon-check-line: | :dsfr&zwj;-icon-check-line: |
| :dsfr-icon-checkbox-circle-fill: | :dsfr&zwj;-icon-checkbox-circle-fill: |
| :dsfr-icon-checkbox-circle-line: | :dsfr&zwj;-icon-checkbox-circle-line: |
| :dsfr-icon-checkbox-fill: | :dsfr&zwj;-icon-checkbox-fill: |
| :dsfr-icon-checkbox-line: | :dsfr&zwj;-icon-checkbox-line: |
| :dsfr-icon-close-circle-fill: | :dsfr&zwj;-icon-close-circle-fill: |
| :dsfr-icon-close-circle-line: | :dsfr&zwj;-icon-close-circle-line: |
| :dsfr-icon-close-line: | :dsfr&zwj;-icon-close-line: |
| :dsfr-icon-delete-bin-fill: | :dsfr&zwj;-icon-delete-bin-fill: |
| :dsfr-icon-delete-bin-line: | :dsfr&zwj;-icon-delete-bin-line: |
| :dsfr-icon-download-fill: | :dsfr&zwj;-icon-download-fill: |
| :dsfr-icon-download-line: | :dsfr&zwj;-icon-download-line: |
| :dsfr-icon-error-warning-fill: | :dsfr&zwj;-icon-error-warning-fill: |
| :dsfr-icon-error-warning-line: | :dsfr&zwj;-icon-error-warning-line: |
| :dsfr-icon-external-link-fill: | :dsfr&zwj;-icon-external-link-fill: |
| :dsfr-icon-external-link-line: | :dsfr&zwj;-icon-external-link-line: |
| :dsfr-icon-eye-fill: | :dsfr&zwj;-icon-eye-fill: |
| :dsfr-icon-eye-line: | :dsfr&zwj;-icon-eye-line: |
| :dsfr-icon-eye-off-fill: | :dsfr&zwj;-icon-eye-off-fill: |
| :dsfr-icon-eye-off-line: | :dsfr&zwj;-icon-eye-off-line: |
| :dsfr-icon-filter-fill: | :dsfr&zwj;-icon-filter-fill: |
| :dsfr-icon-filter-line: | :dsfr&zwj;-icon-filter-line: |
| :dsfr-icon-fr--arrow-left-s-first-line: | :dsfr&zwj;-icon-fr--arrow-left-s-first-line: |
| :dsfr-icon-fr--arrow-left-s-line-double: | :dsfr&zwj;-icon-fr--arrow-left-s-line-double: |
| :dsfr-icon-fr--arrow-right-down-circle-fill: | :dsfr&zwj;-icon-fr--arrow-right-down-circle-fill: |
| :dsfr-icon-fr--arrow-right-s-last-line: | :dsfr&zwj;-icon-fr--arrow-right-s-last-line: |
| :dsfr-icon-fr--arrow-right-s-line-double: | :dsfr&zwj;-icon-fr--arrow-right-s-line-double: |
| :dsfr-icon-fr--arrow-right-up-circle-fill: | :dsfr&zwj;-icon-fr--arrow-right-up-circle-fill: |
| :dsfr-icon-fr--capslock-line: | :dsfr&zwj;-icon-fr--capslock-line: |
| :dsfr-icon-fr--equal-circle-fill: | :dsfr&zwj;-icon-fr--equal-circle-fill: |
| :dsfr-icon-fr--error-fill: | :dsfr&zwj;-icon-fr--error-fill: |
| :dsfr-icon-fr--error-line: | :dsfr&zwj;-icon-fr--error-line: |
| :dsfr-icon-fr--info-fill: | :dsfr&zwj;-icon-fr--info-fill: |
| :dsfr-icon-fr--info-line: | :dsfr&zwj;-icon-fr--info-line: |
| :dsfr-icon-fr--success-fill: | :dsfr&zwj;-icon-fr--success-fill: |
| :dsfr-icon-fr--success-line: | :dsfr&zwj;-icon-fr--success-line: |
| :dsfr-icon-fr--theme-fill: | :dsfr&zwj;-icon-fr--theme-fill: |
| :dsfr-icon-fr--warning-fill: | :dsfr&zwj;-icon-fr--warning-fill: |
| :dsfr-icon-fr--warning-line: | :dsfr&zwj;-icon-fr--warning-line: |
| :dsfr-icon-information-fill: | :dsfr&zwj;-icon-information-fill: |
| :dsfr-icon-information-line: | :dsfr&zwj;-icon-information-line: |
| :dsfr-icon-lock-fill: | :dsfr&zwj;-icon-lock-fill: |
| :dsfr-icon-lock-line: | :dsfr&zwj;-icon-lock-line: |
| :dsfr-icon-lock-unlock-fill: | :dsfr&zwj;-icon-lock-unlock-fill: |
| :dsfr-icon-lock-unlock-line: | :dsfr&zwj;-icon-lock-unlock-line: |
| :dsfr-icon-logout-box-r-fill: | :dsfr&zwj;-icon-logout-box-r-fill: |
| :dsfr-icon-logout-box-r-line: | :dsfr&zwj;-icon-logout-box-r-line: |
| :dsfr-icon-menu-2-fill: | :dsfr&zwj;-icon-menu-2-fill: |
| :dsfr-icon-menu-fill: | :dsfr&zwj;-icon-menu-fill: |
| :dsfr-icon-more-fill: | :dsfr&zwj;-icon-more-fill: |
| :dsfr-icon-more-line: | :dsfr&zwj;-icon-more-line: |
| :dsfr-icon-notification-badge-fill: | :dsfr&zwj;-icon-notification-badge-fill: |
| :dsfr-icon-notification-badge-line: | :dsfr&zwj;-icon-notification-badge-line: |
| :dsfr-icon-question-fill: | :dsfr&zwj;-icon-question-fill: |
| :dsfr-icon-question-line: | :dsfr&zwj;-icon-question-line: |
| :dsfr-icon-refresh-fill: | :dsfr&zwj;-icon-refresh-fill: |
| :dsfr-icon-refresh-line: | :dsfr&zwj;-icon-refresh-line: |
| :dsfr-icon-search-fill: | :dsfr&zwj;-icon-search-fill: |
| :dsfr-icon-search-line: | :dsfr&zwj;-icon-search-line: |
| :dsfr-icon-settings-5-fill: | :dsfr&zwj;-icon-settings-5-fill: |
| :dsfr-icon-settings-5-line: | :dsfr&zwj;-icon-settings-5-line: |
| :dsfr-icon-shield-fill: | :dsfr&zwj;-icon-shield-fill: |
| :dsfr-icon-shield-line: | :dsfr&zwj;-icon-shield-line: |
| :dsfr-icon-star-fill: | :dsfr&zwj;-icon-star-fill: |
| :dsfr-icon-star-line: | :dsfr&zwj;-icon-star-line: |
| :dsfr-icon-star-s-fill: | :dsfr&zwj;-icon-star-s-fill: |
| :dsfr-icon-star-s-line: | :dsfr&zwj;-icon-star-s-line: |
| :dsfr-icon-subtract-line: | :dsfr&zwj;-icon-subtract-line: |
| :dsfr-icon-thumb-down-fill: | :dsfr&zwj;-icon-thumb-down-fill: |
| :dsfr-icon-thumb-down-line: | :dsfr&zwj;-icon-thumb-down-line: |
| :dsfr-icon-thumb-up-fill: | :dsfr&zwj;-icon-thumb-up-fill: |
| :dsfr-icon-thumb-up-line: | :dsfr&zwj;-icon-thumb-up-line: |
| :dsfr-icon-time-fill: | :dsfr&zwj;-icon-time-fill: |
| :dsfr-icon-time-line: | :dsfr&zwj;-icon-time-line: |
| :dsfr-icon-timer-fill: | :dsfr&zwj;-icon-timer-fill: |
| :dsfr-icon-timer-line: | :dsfr&zwj;-icon-timer-line: |
| :dsfr-icon-upload-2-fill: | :dsfr&zwj;-icon-upload-2-fill: |
| :dsfr-icon-upload-2-line: | :dsfr&zwj;-icon-upload-2-line: |
| :dsfr-icon-upload-fill: | :dsfr&zwj;-icon-upload-fill: |
| :dsfr-icon-upload-line: | :dsfr&zwj;-icon-upload-line: |
| :dsfr-icon-zoom-in-fill: | :dsfr&zwj;-icon-zoom-in-fill: |
| :dsfr-icon-zoom-in-line: | :dsfr&zwj;-icon-zoom-in-line: |
| :dsfr-icon-zoom-out-fill: | :dsfr&zwj;-icon-zoom-out-fill: |
| :dsfr-icon-zoom-out-line: | :dsfr&zwj;-icon-zoom-out-line: |

## user

| Icone | Nom |
|-------|-----|
| :dsfr-icon-account-circle-fill: | :dsfr&zwj;-icon-account-circle-fill: |
| :dsfr-icon-account-circle-line: | :dsfr&zwj;-icon-account-circle-line: |
| :dsfr-icon-account-pin-circle-fill: | :dsfr&zwj;-icon-account-pin-circle-fill: |
| :dsfr-icon-account-pin-circle-line: | :dsfr&zwj;-icon-account-pin-circle-line: |
| :dsfr-icon-admin-fill: | :dsfr&zwj;-icon-admin-fill: |
| :dsfr-icon-admin-line: | :dsfr&zwj;-icon-admin-line: |
| :dsfr-icon-group-fill: | :dsfr&zwj;-icon-group-fill: |
| :dsfr-icon-group-line: | :dsfr&zwj;-icon-group-line: |
| :dsfr-icon-parent-fill: | :dsfr&zwj;-icon-parent-fill: |
| :dsfr-icon-parent-line: | :dsfr&zwj;-icon-parent-line: |
| :dsfr-icon-team-fill: | :dsfr&zwj;-icon-team-fill: |
| :dsfr-icon-team-line: | :dsfr&zwj;-icon-team-line: |
| :dsfr-icon-user-add-fill: | :dsfr&zwj;-icon-user-add-fill: |
| :dsfr-icon-user-add-line: | :dsfr&zwj;-icon-user-add-line: |
| :dsfr-icon-user-fill: | :dsfr&zwj;-icon-user-fill: |
| :dsfr-icon-user-heart-fill: | :dsfr&zwj;-icon-user-heart-fill: |
| :dsfr-icon-user-heart-line: | :dsfr&zwj;-icon-user-heart-line: |
| :dsfr-icon-user-line: | :dsfr&zwj;-icon-user-line: |
| :dsfr-icon-user-search-fill: | :dsfr&zwj;-icon-user-search-fill: |
| :dsfr-icon-user-search-line: | :dsfr&zwj;-icon-user-search-line: |
| :dsfr-icon-user-setting-fill: | :dsfr&zwj;-icon-user-setting-fill: |
| :dsfr-icon-user-setting-line: | :dsfr&zwj;-icon-user-setting-line: |
| :dsfr-icon-user-star-fill: | :dsfr&zwj;-icon-user-star-fill: |
| :dsfr-icon-user-star-line: | :dsfr&zwj;-icon-user-star-line: |

## weather

| Icone | Nom |
|-------|-----|
| :dsfr-icon-cloudy-2-fill: | :dsfr&zwj;-icon-cloudy-2-fill: |
| :dsfr-icon-cloudy-2-line: | :dsfr&zwj;-icon-cloudy-2-line: |
| :dsfr-icon-flashlight-fill: | :dsfr&zwj;-icon-flashlight-fill: |
| :dsfr-icon-flashlight-line: | :dsfr&zwj;-icon-flashlight-line: |
| :dsfr-icon-moon-fill: | :dsfr&zwj;-icon-moon-fill: |
| :dsfr-icon-moon-line: | :dsfr&zwj;-icon-moon-line: |
| :dsfr-icon-sun-fill: | :dsfr&zwj;-icon-sun-fill: |
| :dsfr-icon-sun-line: | :dsfr&zwj;-icon-sun-line: |

# Réseaux sociaux

---

## Configuration

Voici un exemple de configuration pour MkDocs DSFR dans le fichier **mkdocs.yml** :

```yaml
---
...

# Theme
...
theme:
  name: dsfr
  ...
  followus:
    facebook: https://www.facebook.com
    youtube: https://www.youtube.com
...
```

Variables de thème :

| Nom                | Valeur par défaut | Description                                  |
|--------------------|-------------------|--------------------------------------------- |
| followus.facebook  | None              | URL vers votre page Facebook                 |
| followus.twitter   | None              | URL vers votre page X (anciennement Twitter) |
| followus.instagram | None              | URL vers votre page Instagram                |
| followus.linkedin  | None              | URL vers votre page LinkedIn                 |
| followus.youtube   | None              | URL vers votre page Youtube                  |

# Pictogrammes - liste

## buildings

| Icone | Nom | Tuile |
|-------|-----|-------|
| :dsfr-picto-buildings-city-hall: | :dsfr&zwj;-picto-buildings-city-hall: | buildings/city-hall |
| :dsfr-picto-buildings-factory: | :dsfr&zwj;-picto-buildings-factory: | buildings/factory |
| :dsfr-picto-buildings-house: | :dsfr&zwj;-picto-buildings-house: | buildings/house |
| :dsfr-picto-buildings-nuclear-plant: | :dsfr&zwj;-picto-buildings-nuclear-plant: | buildings/nuclear-plant |
| :dsfr-picto-buildings-school: | :dsfr&zwj;-picto-buildings-school: | buildings/school |

## digital

| Icone | Nom | Tuile |
|-------|-----|-------|
| :dsfr-picto-digital-application: | :dsfr&zwj;-picto-digital-application: | digital/application |
| :dsfr-picto-digital-avatar: | :dsfr&zwj;-picto-digital-avatar: | digital/avatar |
| :dsfr-picto-digital-calendar: | :dsfr&zwj;-picto-digital-calendar: | digital/calendar |
| :dsfr-picto-digital-coding: | :dsfr&zwj;-picto-digital-coding: | digital/coding |
| :dsfr-picto-digital-data-visualization: | :dsfr&zwj;-picto-digital-data-visualization: | digital/data-visualization |
| :dsfr-picto-digital-internet: | :dsfr&zwj;-picto-digital-internet: | digital/internet |
| :dsfr-picto-digital-mail-send: | :dsfr&zwj;-picto-digital-mail-send: | digital/mail-send |
| :dsfr-picto-digital-search: | :dsfr&zwj;-picto-digital-search: | digital/search |

## document

| Icone | Nom | Tuile |
|-------|-----|-------|
| :dsfr-picto-document-contract: | :dsfr&zwj;-picto-document-contract: | document/contract |
| :dsfr-picto-document-document-add: | :dsfr&zwj;-picto-document-document-add: | document/document-add |
| :dsfr-picto-document-document-download: | :dsfr&zwj;-picto-document-document-download: | document/document-download |
| :dsfr-picto-document-document-signature: | :dsfr&zwj;-picto-document-document-signature: | document/document-signature |
| :dsfr-picto-document-document: | :dsfr&zwj;-picto-document-document: | document/document |
| :dsfr-picto-document-driving-licence: | :dsfr&zwj;-picto-document-driving-licence: | document/driving-licence |
| :dsfr-picto-document-national-identity-card: | :dsfr&zwj;-picto-document-national-identity-card: | document/national-identity-card |
| :dsfr-picto-document-passport: | :dsfr&zwj;-picto-document-passport: | document/passport |
| :dsfr-picto-document-tax-stamp: | :dsfr&zwj;-picto-document-tax-stamp: | document/tax-stamp |
| :dsfr-picto-document-vehicle-registration: | :dsfr&zwj;-picto-document-vehicle-registration: | document/vehicle-registration |

## environment

| Icone | Nom | Tuile |
|-------|-----|-------|
| :dsfr-picto-environment-environment: | :dsfr&zwj;-picto-environment-environment: | environment/environment |
| :dsfr-picto-environment-food: | :dsfr&zwj;-picto-environment-food: | environment/food |
| :dsfr-picto-environment-grocery: | :dsfr&zwj;-picto-environment-grocery: | environment/grocery |
| :dsfr-picto-environment-human-cooperation: | :dsfr&zwj;-picto-environment-human-cooperation: | environment/human-cooperation |
| :dsfr-picto-environment-leaf: | :dsfr&zwj;-picto-environment-leaf: | environment/leaf |
| :dsfr-picto-environment-moon: | :dsfr&zwj;-picto-environment-moon: | environment/moon |
| :dsfr-picto-environment-mountain: | :dsfr&zwj;-picto-environment-mountain: | environment/mountain |
| :dsfr-picto-environment-sun: | :dsfr&zwj;-picto-environment-sun: | environment/sun |
| :dsfr-picto-environment-tree: | :dsfr&zwj;-picto-environment-tree: | environment/tree |

## health

| Icone | Nom | Tuile |
|-------|-----|-------|
| :dsfr-picto-health-health: | :dsfr&zwj;-picto-health-health: | health/health |
| :dsfr-picto-health-hospital: | :dsfr&zwj;-picto-health-hospital: | health/hospital |
| :dsfr-picto-health-vaccine: | :dsfr&zwj;-picto-health-vaccine: | health/vaccine |
| :dsfr-picto-health-virus: | :dsfr&zwj;-picto-health-virus: | health/virus |

## institutions

| Icone | Nom | Tuile |
|-------|-----|-------|
| :dsfr-picto-institutions-firefighter: | :dsfr&zwj;-picto-institutions-firefighter: | institutions/firefighter |
| :dsfr-picto-institutions-gendarmerie: | :dsfr&zwj;-picto-institutions-gendarmerie: | institutions/gendarmerie |
| :dsfr-picto-institutions-justice: | :dsfr&zwj;-picto-institutions-justice: | institutions/justice |
| :dsfr-picto-institutions-money: | :dsfr&zwj;-picto-institutions-money: | institutions/money |
| :dsfr-picto-institutions-police: | :dsfr&zwj;-picto-institutions-police: | institutions/police |

## leisure

| Icone | Nom | Tuile |
|-------|-----|-------|
| :dsfr-picto-leisure-book: | :dsfr&zwj;-picto-leisure-book: | leisure/book |
| :dsfr-picto-leisure-community: | :dsfr&zwj;-picto-leisure-community: | leisure/community |
| :dsfr-picto-leisure-culture: | :dsfr&zwj;-picto-leisure-culture: | leisure/culture |
| :dsfr-picto-leisure-digital-art: | :dsfr&zwj;-picto-leisure-digital-art: | leisure/digital-art |
| :dsfr-picto-leisure-paint: | :dsfr&zwj;-picto-leisure-paint: | leisure/paint |

## map

| Icone | Nom | Tuile |
|-------|-----|-------|
| :dsfr-picto-map-airport: | :dsfr&zwj;-picto-map-airport: | map/airport |
| :dsfr-picto-map-location-france: | :dsfr&zwj;-picto-map-location-france: | map/location-france |
| :dsfr-picto-map-luggage: | :dsfr&zwj;-picto-map-luggage: | map/luggage |
| :dsfr-picto-map-map: | :dsfr&zwj;-picto-map-map: | map/map |

## system

| Icone | Nom | Tuile |
|-------|-----|-------|
| :dsfr-picto-system-connection-lost: | :dsfr&zwj;-picto-system-connection-lost: | system/connection-lost |
| :dsfr-picto-system-error: | :dsfr&zwj;-picto-system-error: | system/error |
| :dsfr-picto-system-information: | :dsfr&zwj;-picto-system-information: | system/information |
| :dsfr-picto-system-notification: | :dsfr&zwj;-picto-system-notification: | system/notification |
| :dsfr-picto-system-padlock: | :dsfr&zwj;-picto-system-padlock: | system/padlock |
| :dsfr-picto-system-success: | :dsfr&zwj;-picto-system-success: | system/success |
| :dsfr-picto-system-system: | :dsfr&zwj;-picto-system-system: | system/system |
| :dsfr-picto-system-technical-error: | :dsfr&zwj;-picto-system-technical-error: | system/technical-error |
| :dsfr-picto-system-warning: | :dsfr&zwj;-picto-system-warning: | system/warning |

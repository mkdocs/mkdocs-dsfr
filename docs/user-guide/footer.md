# Pied de page

---

## Configuration

Voici un exemple de configuration pour MkDocs DSFR dans le fichier **mkdocs.yml** :

```yaml
---
...

# Theme
...
theme:
  name: dsfr
  ...
  footer:
    description: Description exemple
    links:
      - name: legifrance.gouv.fr
        url: https://legifrance.gouv.fr
      - name: gouvernement.fr
        url: https://gouvernement.fr
      - name: service-public.fr
        url: https://service-public.fr
      - name: data.gouv.fr
        url: https://data.gouv.fr
...
```

Variables de thème :

|        Nom         |           Valeur par défaut           |       Description        |
| ------------------ | ------------------------------------- | ------------------------ |
| footer.description | None                                  | Description du footer    |
| footer.links       | (Voir liste dans l'exemple ci-dessus) | Liste de liens du footer |

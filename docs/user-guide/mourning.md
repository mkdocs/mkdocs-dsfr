# Périodes de deuil national

---

## Configuration

Voici un exemple de configuration pour MkDocs DSFR dans le fichier **mkdocs.yml** :

```yaml
---
...

# Theme
...
theme:
  name: dsfr
  ...
  global:
    mourning: true
...
```

Variables de thème :

|       Nom       | Valeur par défaut |                    Description                     |
| --------------- | ----------------- | -------------------------------------------------- |
| global.mourning | false             | Version en berne de la Marianne (header et footer) |

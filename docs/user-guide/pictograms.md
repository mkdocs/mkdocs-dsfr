# Pictogrammes :dsfr-picto-buildings-nuclear-plant:

---

## Configuration

Aucune configuration n'est requise, les pictogrammes sont intégrés par le thème DSFR.

## Utilisation

Pour utiliser les pictogrammes du DSFR, il suffit d'utiliser la syntaxe suivante dans un fichier MarkDown : <b>:dsfr&zwj;-picto-categorie-nom pictogramme:</b>.

Par exemple, le texte <b>:dsfr&zwj;-picto-digital-application:</b> sera remplacé par : :dsfr-picto-digital-application:.

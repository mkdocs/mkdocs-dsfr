# Couleurs - liste

| Couleur | Nom | Code |
|---------|-----|------|
| <p style="background-color:#6a6af4; height: 1.5em;"></p> | blue-france | #6a6af4 |
| <p style="background-color:#e1000f; height: 1.5em;"></p> | red-marianne | #e1000f |
| <p style="background-color:#b7a73f; height: 1.5em;"></p> | green-tilleul-verveine | #b7a73f |
| <p style="background-color:#68a532; height: 1.5em;"></p> | green-bourgeon | #68a532 |
| <p style="background-color:#00a95f; height: 1.5em;"></p> | green-emeraude | #00a95f |
| <p style="background-color:#009081; height: 1.5em;"></p> | green-menthe | #009081 |
| <p style="background-color:#009099; height: 1.5em;"></p> | green-archipel | #009099 |
| <p style="background-color:#465f9d; height: 1.5em;"></p> | blue-ecume | #465f9d |
| <p style="background-color:#417dc4; height: 1.5em;"></p> | blue-cumulus | #417dc4 |
| <p style="background-color:#a558a0; height: 1.5em;"></p> | purple-glycine | #a558a0 |
| <p style="background-color:#e18b76; height: 1.5em;"></p> | pink-macaron | #e18b76 |
| <p style="background-color:#ce614a; height: 1.5em;"></p> | pink-tuile | #ce614a |
| <p style="background-color:#c8aa39; height: 1.5em;"></p> | yellow-tournesol | #c8aa39 |
| <p style="background-color:#c3992a; height: 1.5em;"></p> | yellow-moutarde | #c3992a |
| <p style="background-color:#e4794a; height: 1.5em;"></p> | orange-terre-battue | #e4794a |
| <p style="background-color:#d1b781; height: 1.5em;"></p> | brown-cafe-creme | #d1b781 |
| <p style="background-color:#c08c65; height: 1.5em;"></p> | brown-caramel | #c08c65 |
| <p style="background-color:#bd987a; height: 1.5em;"></p> | brown-opera | #bd987a |
| <p style="background-color:#aea397; height: 1.5em;"></p> | beige-gris-galet | #aea397 |
| <p style="background-color:#6a6af4; height: 1.5em;"></p> | blue-france | #6a6af4 |
| <p style="background-color:#e1000f; height: 1.5em;"></p> | red-marianne | #e1000f |
| <p style="background-color:#b7a73f; height: 1.5em;"></p> | green-tilleul-verveine | #b7a73f |
| <p style="background-color:#68a532; height: 1.5em;"></p> | green-bourgeon | #68a532 |
| <p style="background-color:#00a95f; height: 1.5em;"></p> | green-emeraude | #00a95f |
| <p style="background-color:#009081; height: 1.5em;"></p> | green-menthe | #009081 |
| <p style="background-color:#009099; height: 1.5em;"></p> | green-archipel | #009099 |
| <p style="background-color:#465f9d; height: 1.5em;"></p> | blue-ecume | #465f9d |
| <p style="background-color:#417dc4; height: 1.5em;"></p> | blue-cumulus | #417dc4 |
| <p style="background-color:#a558a0; height: 1.5em;"></p> | purple-glycine | #a558a0 |
| <p style="background-color:#e18b76; height: 1.5em;"></p> | pink-macaron | #e18b76 |
| <p style="background-color:#ce614a; height: 1.5em;"></p> | pink-tuile | #ce614a |
| <p style="background-color:#c8aa39; height: 1.5em;"></p> | yellow-tournesol | #c8aa39 |
| <p style="background-color:#c3992a; height: 1.5em;"></p> | yellow-moutarde | #c3992a |
| <p style="background-color:#e4794a; height: 1.5em;"></p> | orange-terre-battue | #e4794a |
| <p style="background-color:#d1b781; height: 1.5em;"></p> | brown-cafe-creme | #d1b781 |
| <p style="background-color:#c08c65; height: 1.5em;"></p> | brown-caramel | #c08c65 |
| <p style="background-color:#bd987a; height: 1.5em;"></p> | brown-opera | #bd987a |
| <p style="background-color:#aea397; height: 1.5em;"></p> | beige-gris-galet | #aea397 |

# Balises de Code Markdown

---

## Utilisation Courante

Lors de la création de votre projet Mkdocs, l'utilisation de [balises de code Markdown](https://docs.github.com/en/get-started/writing-on-github/working-with-advanced-formatting/creating-and-highlighting-code-blocks) est une pratique fréquente. Voici quelques exemples concrets pour vous aider à comprendre leur application :

```yaml
# Exemple d'utilisation de la balise ```yaml```
theme:
  name: dsfr

plugins:
  - search
  - dsfr-plugin
```

## Personnalisation de `highlightjs`

Si vous souhaitez ajuster la version ou le thème par défaut, il vous suffit d'ajouter le paramètre `highlightjs` dans le fichier `mkdocs.yml`, comme illustré ci-dessous :

```yaml
# Configuration du Thème
theme:
  name: dsfr
  highlightjs:
    version: 11.10.0
    theme_light: base16/solar-flare
    theme_dark: base16/solar-flare-light
```

| Nom                     | Valeur par défaut        | Description                                       |
|-------------------------|--------------------------|---------------------------------------------------|
| highlightjs.version     | 11.10.0                  | Version highlightjs                               |
| highlightjs.theme_light | base16/solar-flare       | Theme highlightjs pour le light theme MkDocs DSFR |
| highlightjs.theme_dark  | base16/solar-flare-light | Theme highlightjs pour le dark theme MkDocs DSFR  |

Exemple de modification de la configuration :

```yaml
# Configuration du Thème
theme:
  name: dsfr
  highlightjs:
    version: 11.9.0
    theme_light: github-dark
    theme_dark: github
```

Pour découvrir des exemples de thèmes disponibles, n'hésitez pas à consulter [ce lien](https://highlightjs.org/examples).

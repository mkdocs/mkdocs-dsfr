# Guide Utilisateur

---

```dsfr-plugin-tile
group:
  - title: Balises de Code Markdown
    link: code
    pictogram: digital/coding
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Composants
    link: components
    pictogram: system/system
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Emoticones
    link: emojis
    pictogram: leisure/paint
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Icônes
    link: icons
    pictogram: digital/avatar
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Pictogrammes
    link: pictograms
    pictogram: buildings/nuclear-plant
    col_md: 3
    margin_x: 1
    margin_y: 1
```

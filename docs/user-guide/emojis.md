# Emoticones :smile:

---

## Configuration

Intégrez la configuration suivante dans le fichier `mkdocs.yml` pour activer la fonctionnalité Emoji :

```yaml
# Extensions Markdown
markdown_extensions:
  - pymdownx.emoji:
      emoji_generator: !!python/name:pymdownx.emoji.to_svg
```

## Utilisation

Après avoir ajouté la configuration ci-dessus, vous pourrez facilement utiliser des emojis classiques tels que :smile:, :heart:, ou encore :thumbsup:.

Consultez la [liste](https://gist.github.com/rxaviers/7360908) des emojis disponibles pour faire votre choix.

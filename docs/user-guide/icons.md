# Icones :dsfr-icon-virus-line:

---

## Configuration

Aucune configuration n'est requise, les icones sont intégrés par le thème DSFR.

## Utilisation

Pour utiliser les icones du DSFR, il suffit d'utiliser la syntaxe suivante dans un fichier MarkDown : <b>:dsfr&zwj;-icon-nom-icone:</b>.

Par exemple, le texte <b>:dsfr&zwj;-icon-phone-line:</b> sera remplacé par : :dsfr-icon-phone-line:.

### :dsfr-icon-phone-line: Exemple de titre avec une icone

## Couleurs

Il est possible d'ajouter une couleur à l'icone avec la syntaxe <b>:dsfr&zwj;-icon-nom-icone|couleur:</b>.

Par exemple, le texte <b>:dsfr&zwj;-icon-phone-line|red-marianne:</b> sera remplacé par : :dsfr-icon-phone-line|red-marianne:.

La liste des couleurs est disponible [ici](colors-names.md)

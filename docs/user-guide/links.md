# Liens

---

## Lien de téléchargement

Pour utiliser les liens de téléchargement du DSFR, il suffit d'ajouter l'extension markdown `attr_list` dans votre fichier `mkdocs.yml` comme suit :

```yaml
...

# Markdown extensions
markdown_extensions:
  - attr_list

...
```

Ensuite vous pouvez ajouter un lien de la manière suivante :

```markdown
[Télécharger le Fichier](chemin/relatif/vers/le/fichier){:download}
```

Voici un exemple de résultat : [Test Download](../assets/files/test-download.txt){:download}

# Highlights (Mise en exergue)

---

## Présentation

La mise en exergue permet à l’utilisateur de distinguer et repérer une information facilement.

Consultez la documentation complète de la mise en exergue DSFR en suivant ce [lien](https://www.systeme-de-design.gouv.fr/composants-et-modeles/composants/mise-en-exergue/)

## Exemple

Voici un aperçu du composant **highlight** DSFR :

```dsfr-plugin-highlight
content: Description
```

## Paramètres

Utilisez les paramètres suivants pour personnaliser la mise en exergue :

- **content** : Contenu de la mise en exergue
- **size** : Taille du texte avec comme valeur `sm`, `md` ou `lg` (par défaut `md`)
- **color** : Couleur de la mise en exergue (TODO ajouter une liste de couleur)

```yml
content: Description
size: md
color: green-menthe
```

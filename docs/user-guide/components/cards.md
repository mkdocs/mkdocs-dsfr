# Cards (Cartes)

---

## Présentation

La carte est un lien vers une page éditoriale dont elle donne un aperçu.

Consultez la documentation complète d'une [carte](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/carte) DSFR et des [medias](https://www.systeme-de-design.gouv.fr/elements-d-interface/fondamentaux-techniques/medias).

## Exemple

Voici un aperçu du composant **card** DSFR :

```dsfr-plugin-card
group:
  - title: Titre1
    description: Description1
    image: assets/images/mkdocs.png
    image_ratio: 1x1
    labels:
      - name: Google
        link: https://google.fr
      - name: Système de Design de l'État
        link: https://www.systeme-de-design.gouv.fr
    col_md: 2
    margin_x: 1
    margin_y: 1

  - title: Title2
    description: Description2
    link: https://www.systeme-de-design.gouv.fr
    image: assets/images/mkdocs.png
    image_ratio: 1x1
    col_md: 2
    margin_x: 1
    margin_y: 1

  - title: Title3
    description: Description3
    image: assets/images/mkdocs.png
    image_ratio: 1x1
    col_md: 2
    margin_x: 1
    margin_y: 1

  - title: Title4
    description: Description4
    image: assets/images/mkdocs.png
    image_ratio: 1x1
    col_md: 2
    margin_x: 1
    margin_y: 1

  - title: Title5
    description: Description5
    image: assets/images/mkdocs.png
    image_ratio: 1x1
    col_md: 2
    margin_x: 1
    margin_y: 1
```

## Paramètres

Utilisez les paramètres suivants pour personnaliser la carte :

- **title** : Titre de la carte
- **description** : Description de la carte
- **image** : Lien vers le fichier de l'image
- **image_ratio** : Ratio de l'image (16x9 par défaut)
- **labels** : Liste de labels et de liens
- **col_md** : Taille de la carte de 1 à 12 (par défaut 3)
- **margin_x** : Margin left et right (aucun par défaut)
- **margin_y** : Margin top et bottom (aucun par défaut)

```yml
group:
  - title: Titre1
    description: Description1
    image: assets/images/mkdocs.png
    image_ratio: 1x1
    labels:
      - name: Google
        link: https://google.fr
      - name: Système de Design de l'État
        link: https://www.systeme-de-design.gouv.fr
    col_md: 2
    margin_x: 1
    margin_y: 1

  - title: Title2
    description: Description2
    link: https://www.systeme-de-design.gouv.fr
    image: assets/images/mkdocs.png
    image_ratio: 1x1
    col_md: 2
    margin_x: 1
    margin_y: 1

  - title: Title3
    description: Description3
    image: assets/images/mkdocs.png
    image_ratio: 1x1
    col_md: 2
    margin_x: 1
    margin_y: 1

  - title: Title4
    description: Description4
    image: assets/images/mkdocs.png
    image_ratio: 1x1
    col_md: 2
    margin_x: 1
    margin_y: 1

  - title: Title5
    description: Description5
    image: assets/images/mkdocs.png
    image_ratio: 1x1
    col_md: 2
    margin_x: 1
    margin_y: 1
```

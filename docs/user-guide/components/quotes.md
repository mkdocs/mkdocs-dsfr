# Quotes (Citations)

---

## Présentation

La citation permet de citer un texte dans une page éditoriale. La citation peut provenir d'un extrait d’un discours oral formulé par une tierce personne ou d’un texte écrit.

Consultez la documentation complète de la citation DSFR en suivant ce [lien](https://www.systeme-de-design.gouv.fr/composants-et-modeles/composants/citation).

## Exemple

Voici un aperçu du composant **quote** DSFR :

```dsfr-plugin-quote
text: "Lorem [...] elit ut."
source_url: https://example.com
author: Auteur
details:
  - text: Ouvrage
    is_title: true
  - text: Détail 1
  - text: Détail 2
  - text: Détail 3
  - text: Détail 4
    source_url: https://example.com
image: assets/images/mkdocs.png
color: orange-terre-battue
```

## Paramètres

Utilisez les paramètres suivants pour personnaliser la citation :

- **text** : Le texte contenant la citation
- **source_url** : L'URL de la citation
- **author**: Le nom de l'auteur
- **details**: Liste des détails
- **image**: Lien vers le fichier de l'image
- **color**: Couleur du logo de citation (TODO ajouter une liste de couleur)

```yml
text: "Lorem [...] elit ut."
source_url: https://example.com
author: Auteur
details:
  - text: Ouvrage
    is_title: true
  - text: Détail 1
  - text: Détail 2
  - text: Détail 3
  - text: Détail 4
    source_url: https://example.com
image: assets/images/mkdocs.png
color: orange-terre-battue
```

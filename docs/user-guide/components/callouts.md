# Callouts (Mise en avant)

---

## Présentation

La mise en avant permet à l’utilisateur de distinguer rapidement une information qui vient compléter le contenu consulté.

Consultez la documentation complète d'une mise en avant DSFR en suivant ce [lien](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/mise-en-avant)

## Exemple

Voici un aperçu du composant **callout** DSFR :

```dsfr-plugin-callout
title: Title1
content: "Lorem ipsum dolor sit amet. Eum minima error et harum sunt vel explicabo dolor est molestias rerum ut quaerat vero ut facere aperiam est explicabo officiis. Hic tenetur similique ad iste voluptatibus ex sapiente tempore aut explicabo dolore ut molestiae pariatur qui autem omnis? A sunt aliquam qui consequuntur quia quo excepturi atque eos sunt minus ea praesentium internos."
```

```dsfr-plugin-callout
title: Title2
content: "Lorem ipsum dolor sit amet. Eum minima error et harum sunt vel explicabo dolor est molestias rerum ut quaerat vero ut facere aperiam est explicabo officiis. Hic tenetur similique ad iste voluptatibus ex sapiente tempore aut explicabo dolore ut molestiae pariatur qui autem omnis? A sunt aliquam qui consequuntur quia quo excepturi atque eos sunt minus ea praesentium internos."
color: brown-caramel
```

## Paramètres

Utilisez les paramètres suivants pour personnaliser la mise en avant :

- **title** : Titre de la mise en avant
- **content** : Description de la mise en avant
- **color** :  Couleur de la mise en avant (TODO ajouter une liste de couleur)

```yml
title: Title1
content: "Lorem ipsum dolor sit amet. Eum minima error et harum sunt vel explicabo dolor est molestias rerum ut quaerat vero ut facere aperiam est explicabo officiis. Hic tenetur similique ad iste voluptatibus ex sapiente tempore aut explicabo dolore ut molestiae pariatur qui autem omnis? A sunt aliquam qui consequuntur quia quo excepturi atque eos sunt minus ea praesentium internos."
color: brown-caramel
```

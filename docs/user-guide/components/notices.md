# Notices (Bandeau d'information importante)

---

## Présentation

Le bandeau d’information importante permet aux utilisateurs de voir ou d’accéder à une information importante et temporaire.

Consultez la documentation complète d'un bandeau d’information importante DSFR en suivant ce [lien](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/bandeau-d-information-importante)

## Exemple

Voici un aperçu du composant **notice** DSFR :

```dsfr-plugin-notice
title: Description1
```

## Paramètres

Utilisez les paramètres suivants pour personnaliser le bandeau d’information importante :

- **title** : Titre du bandeau d’information importante

```yml
title: Description1
```

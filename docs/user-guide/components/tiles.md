# Tiles (Tuiles)

---

## Présentation

La tuile est un raccourci ou point d’entrée qui redirige les utilisateurs vers des pages de contenu.

Consultez la documentation complète d'une tuile DSFR en suivant ce [lien](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/tuile).

## Exemple

Voici un aperçu du composant **tile** DSFR :

```dsfr-plugin-tile
group:
  - title: Titre1
    description: Description1
    details: Détails1
    link: "#"
    pictogram: buildings/city-hall
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Titre2
    description: Description2
    details: Détails2
    link: "#"
    pictogram: buildings/city-hall
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Titre3
    description: Description3
    details: Détails3
    link: "#"
    pictogram: buildings/city-hall
    col_md: 3
    margin_x: 1
    margin_y: 1
```

## Paramètres

Utilisez les paramètres suivants pour personnaliser la tuile :

- **title** : Titre de la tuile
- **description** : Description de la tuile
- **details** :  Une seconde description plus concise affichée sous la principale
- **link** : Lien de redirection en cliquant sur la tuile
- **pictogram** : Nom du pictogramme affiché sur la tuile composé par **catégorie/nom** ([liste pictogrammes](../pictograms-names.md))
- **col_md** : Taille de la tuile de 1 à 12 (par défaut 3)
- **margin_x** : Margin left et right (aucun par défaut)
- **margin_y** : Margin top et bottom (aucun par défaut)

```yml
group:
  - title: Titre1
    description: Description1
    details: Détails1
    link: "#"
    pictogram: buildings/city-hall
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Titre2
    description: Description2
    details: Détails2
    link: "#"
    pictogram: buildings/city-hall
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Titre3
    description: Description3
    details: Détails3
    link: "#"
    pictogram: buildings/city-hall
    col_md: 3
    margin_x: 1
    margin_y: 1
```

# Badges

---

## Présentation

Le composant badge permet de mettre en avant une information de type **statut** ou **état** sur un élément du site.

Consultez la documentation complète d'un badge DSFR en suivant ce [lien](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/badge)

## Exemple

Voici un aperçu du composant **badge** DSFR :

```dsfr-plugin-badge
group:
  - type: success
    title: Label

  - type: error
    title: Label

  - type: info
    title: Label

  - type: warning
    title: Label

  - type: new
    title: Label

  - color: brown-cafe-creme
    title: Label

  - type: new
    no_icon: true
    color: green-menthe
    sm: true
    title: Label
```

## Paramètres

Utilisez les paramètres suivants pour personnaliser la tuile :

- **type** : Type du badge (**success**, **error**, **info**, **warning**, **new**)
- **title** : Titre du badge
- **no_icon**: Désactive l'affichage de l'icone (true ou false)
- **color** : Couleur du badge (TODO ajouter une liste de couleur)
- **sm** : Active la petite taille du badge (true ou false)

```yml
group:
  - type: new
    no_icon: true
    color: green-menthe
    sm: true
    title: Label
```

# Tabs (Onglets)

---

## Présentation

Le composant onglet permet aux utilisateurs de naviguer dans différentes sections de contenu au sein d’une même page.

Consultez la documentation complète d'un onglet DSFR en suivant ce [lien](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/onglet)

## Exemple

Voici un aperçu du composant **tab** DSFR :

```dsfr-plugin-tab
group:
  - title: Titre1
    content: Description1

  - title: Titre2
    content: Description2

  - title: Titre3
    content: Description3
```

## Paramètres

Utilisez les paramètres suivants pour personnaliser l'onglet :

- **title** : Titre de l'onglet
- **content** : Contenu de l'onglet

```yml
group:
  - title: Titre1
    content: Description1

  - title: Titre2
    content: Description2

  - title: Titre3
    content: Description3
```

# Alerts (Alertes)

---

## Présentation

Les alertes permettent d’attirer l’attention de l’utilisateur sur une information sans interrompre sa tâche en cours.

Consultez la documentation complète d'une alerte DSFR en suivant ce [lien](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/alerte/)

## Exemple

Voici un aperçu du composant **alert** DSFR :

```dsfr-plugin-alert
type: error
title: "Error: title"
description: Description
```

```dsfr-plugin-alert
type: success
title: "Success: title"
description: Description
```

```dsfr-plugin-alert
type: info
title: "Info: title"
description: Description
```

```dsfr-plugin-alert
type: warning
title: "Warning: title"
description: Description
```

## Paramètres

Utilisez les paramètres suivants pour personnaliser l'alerte :

- **type**: Type de l'alerte
- **title** : Titre de l'alerte
- **description** : Description de l'alerte

```yml
type: error
title: "Error: title"
description: Description
```

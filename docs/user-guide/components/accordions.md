# Accordions (Accordéons)

---

## Présentation

Les accordéons permettent aux utilisateurs d'afficher et de masquer des sections de contenu présentés dans une page.

Consultez la documentation complète d'un accordéon DSFR en suivant ce [lien](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/accordeon)

## Exemple

Voici un aperçu du composant **accordion** DSFR :

```dsfr-plugin-accordion
group:
  - title: Titre1
    content: Description1

  - title: Titre2
    content: Description2

  - title: Titre3
    content: Description3

  - title: Titre4
    content: Description4

  - title: Titre5
    content: Description5
```

## Paramètres

Utilisez les paramètres suivants pour personnaliser l'accordéon :

- **title** : Titre de l'accordéon
- **content** : Contenu de l'accordéon

```yml
group:
  - title: Titre1
    content: Description1

  - title: Titre2
    content: Description2

  - title: Titre3
    content: Description3

  - title: Titre4
    content: Description4

  - title: Titre5
    content: Description5
```

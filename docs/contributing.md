# Contribuer

---

## Création d'un problème (Issue)

```dsfr-plugin-tile
group:
  - title: Quelque chose ne fonctionne pas ?
    description: Signaler un bug dans MkDocs DSFR en créant un problème (issue)
    link: https://gitlab.mim-libre.fr/mkdocs/mkdocs-dsfr/-/issues/new?issuable_template=issue&issue[title]=[Bug]
    pictogram: system/error
    col_md: 5
    margin_x: 1
    margin_y:

  - title: Informations manquantes dans la documentation ?
    description: Signaler les informations manquantes ou les incohérences potentielles dans la documentation
    link: https://gitlab.mim-libre.fr/mkdocs/mkdocs-dsfr/-/issues/new?issuable_template=issue&issue[title]=[Doc]
    pictogram: document/document-add
    col_md: 5
    margin_x: 1
    margin_y: 1

  - title: Vous souhaitez soumettre une idée ?
    description: Proposer un changement, une demande de fonctionnalité ou suggérer une amélioration
    link: https://gitlab.mim-libre.fr/mkdocs/mkdocs-dsfr/-/issues/new?issuable_template=issue&issue[title]=[Feature]
    pictogram: system/information
    col_md: 5
    margin_x: 1
    margin_y: 1

  - title: Vous avez une question ou besoin d'aide ?
    description: Posez une question sur notre forum de discussion et entrez en contact avec notre communauté
    link: https://gitlab.mim-libre.fr/mkdocs/mkdocs-dsfr/-/issues/new?issuable_template=issue&issue[title]=[Question]
    pictogram: leisure/community
    col_md: 5
    margin_x: 1
    margin_y: 1
```

## Code de conduite (Code of Conduct)

TODO

## Demandes de tirage (Pull Request)

TODO

## Conventional Commit

Nous suivons la convention de commit standard, connue sous le nom de **Conventional Commit**, pour tous nos commits dans ce projet. Cette approche de notation des messages de commit nous aide à maintenir une structure claire et compréhensible pour le suivi des changements.

Lors de vos contributions, veuillez vous assurer de respecter la convention de commit standard. Pour plus d'informations sur la convention Conventional Commit, consultez [le guide officiel](https://www.conventionalcommits.org/fr/v1.0.0/).

## Installation pour le développement

### Prérequis

Avant de commencer le développement, assurez-vous d'avoir les éléments suivants installés sur votre machine :

- [Python](https://www.python.org): Version 3.12 ou supérieure
- [Poetry](https://python-poetry.org/) Version 1.8.4 ou supérieure
- [Task](https://taskfile.dev): Version 3.39.2 ou supérieure

### Étapes d'Installation

Après l'installation des prérequis, veuillez exécuter les commandes suivantes :

```bash
git clone url_du_projet.git
cd projet
task init
task help
```

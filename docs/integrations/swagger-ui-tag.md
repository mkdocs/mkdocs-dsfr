# Swagger UI Tag

---

## Présentation

[MkDocs Swagger UI Tag](https://blueswen.github.io/mkdocs-swagger-ui-tag/) permet d'intégrer [Swagger UI](https://github.com/swagger-api/swagger-ui) sur une page.

## Exemple d'intégration DSFR

<swagger-ui src="https://petstore.swagger.io/v2/swagger.json"/>

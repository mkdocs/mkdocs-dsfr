# Kroki

---

## Présentation

[mkdocs-kroki-plugin](https://github.com/AVATEAM-IT-SYSTEMHAUS/mkdocs-kroki-plugin) permet d'intégrer des schémas [Kroki](https://github.com/yuzutech/kroki) sur une page. On peut trouver sur la [documentation de Kroki](https://kroki.io/#try) des exemples de schémas.

## Exemple d'intégration DSFR

```kroki-mermaid
classDiagram
Class01 <|-- AveryLongClass : Cool
<<Interface>> Class01
Class09 --> C2 : Where am I?
Class09 --* C3
Class09 --|> Class07
Class07 : equals()
Class07 : Object[] elementData
Class01 : size()
Class01 : int chimp
Class01 : int gorilla
class Class10 {
  <<service>>
  int id
  size()
}

```

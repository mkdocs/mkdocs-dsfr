# Awesome Pages

---

## Présentation

[Awesome Pages](https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin) vous permet de personnaliser la façon dont vos pages s'affichent dans la navigation de votre **MkDocs** sans avoir à configurer l'ensemble de la structure dans votre **mkdocs.yml**. Il vous offre un contrôle détaillé en utilisant un petit fichier de configuration directement placé dans le répertoire pertinent de votre documentation.

## Exemple d'intégration DSFR

Ce plugin est utilisé dans **MkDocs DFSR** pour réorganiser l'ordre des pages principales :

```yaml
# docs/.pages
---
nav:
  - index.md
  - installation.md
  - user-guide
  - contributing.md
  - integrations
```

# MkDocs DSFR

Thème [MkDocs](https://www.mkdocs.org/) intégrant le DSFR.

---

```dsfr-plugin-tile
group:
  - title: Installation
    link: installation
    pictogram: system/information
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Guide Utilisateur
    link: user-guide
    pictogram: leisure/book
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Contribution
    link: contributing
    pictogram: environment/human-cooperation
    col_md: 3
    margin_x: 1
    margin_y: 1

  - title: Intégration
    link: integrations
    pictogram: system/system
    col_md: 3
    margin_x: 1
    margin_y: 1
```

#!/bin/bash

rm -f docs/user-guide/icons-names.md
touch docs/user-guide/icons-names.md
echo "# Icones - liste" >> docs/user-guide/icons-names.md

icon_categories=$(ls src/mkdocs_dsfr/theme/dsfr_assets/icons/)

for category in $icon_categories; do
    echo "" >> docs/user-guide/icons-names.md
    echo "## $category" >> docs/user-guide/icons-names.md
    echo "" >> docs/user-guide/icons-names.md
    echo "| Icone | Nom |" >> docs/user-guide/icons-names.md
    echo "|-------|-----|" >> docs/user-guide/icons-names.md

    icons=$(ls src/mkdocs_dsfr/theme/dsfr_assets/icons/"$category")

    for icon in $icons; do
    icon_without_extension=$(echo "$icon" | cut -d. -f1)
    echo "| :dsfr-icon-$icon_without_extension: | :dsfr&zwj;-icon-$icon_without_extension: |" >> docs/user-guide/icons-names.md
    done
done

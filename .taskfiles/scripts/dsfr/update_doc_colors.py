"""Script to update colors documentation (see docs/user-guide/colors-names.md)."""

import os
import re


pattern = re.compile(r"--([a-z]+.+)-main-[0-9]+:.\#(.{6});")
dsfr_css_file_path = "src/mkdocs_dsfr/theme/dsfr_assets/dsfr.css"
output_file_path = "docs/user-guide/colors-names.md"

if os.path.exists(output_file_path):
    os.remove(output_file_path)

with open(dsfr_css_file_path, "r") as file:
    content = file.read()

results = pattern.findall(content)

with open(output_file_path, "w") as output_file:
    output_file.write("# Couleurs - liste\n\n")
    output_file.write("| Couleur | Nom | Code |\n")
    output_file.write("|---------|-----|------|\n")

    for result in results:
        color_name, color_code = result
        print("Groupe 1:", color_name)
        print("Groupe 2:", color_code)
        line = f"| <p style=\"background-color:#{color_code}; height: 1.5em;\"></p> | {color_name} | #{color_code} |\n"
        output_file.write(line)

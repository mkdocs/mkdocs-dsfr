#!/bin/bash

rm -f docs/user-guide/pictograms-names.md
touch docs/user-guide/pictograms-names.md
echo "# Pictogrammes - liste" >> docs/user-guide/pictograms-names.md

picto_categories=$(ls src/mkdocs_dsfr/theme/dsfr_assets/artwork/pictograms)

for category in $picto_categories; do
    echo "" >> docs/user-guide/pictograms-names.md
    echo "## $category" >> docs/user-guide/pictograms-names.md
    echo "" >> docs/user-guide/pictograms-names.md
    echo "| Icone | Nom | Tuile |" >> docs/user-guide/pictograms-names.md
    echo "|-------|-----|-------|" >> docs/user-guide/pictograms-names.md

    pictos=$(ls src/mkdocs_dsfr/theme/dsfr_assets/artwork/pictograms/"$category")

    for picto in $pictos; do
    picto_without_extension=$(echo "$picto" | cut -d. -f1)
    echo "| :dsfr-picto-$category-$picto_without_extension: | :dsfr&zwj;-picto-$category-$picto_without_extension: | $category/$picto_without_extension |" >> docs/user-guide/pictograms-names.md
    done
done

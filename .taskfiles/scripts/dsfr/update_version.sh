#!/bin/bash

if [[ $# -ne 1 ]]; then
    echo "Usage: $0 <dsfr-version>"
    exit 1
fi

DSFR_VERSION=$1

mkdir -p /tmp/dsfr-$DSFR_VERSION
wget -P /tmp/dsfr-$DSFR_VERSION https://github.com/GouvernementFR/dsfr/releases/download/v$DSFR_VERSION/dsfr-v$DSFR_VERSION.zip
unzip -o /tmp/dsfr-$DSFR_VERSION/dsfr-v$DSFR_VERSION.zip -d /tmp/dsfr-$DSFR_VERSION
rm -rf src/mkdocs_dsfr/theme/dsfr_assets/*
cp /tmp/dsfr-$DSFR_VERSION/dist/dsfr.css src/mkdocs_dsfr/theme/dsfr_assets/
cp /tmp/dsfr-$DSFR_VERSION/dist/dsfr.module.js src/mkdocs_dsfr/theme/dsfr_assets/
cp /tmp/dsfr-$DSFR_VERSION/dist/dsfr.module.js.map src/mkdocs_dsfr/theme/dsfr_assets/
cp /tmp/dsfr-$DSFR_VERSION/dist/favicon/favicon.svg src/mkdocs_dsfr/theme/dsfr_assets/
cp /tmp/dsfr-$DSFR_VERSION/dist/utility/utility.css src/mkdocs_dsfr/theme/dsfr_assets/
cp -r /tmp/dsfr-$DSFR_VERSION/dist/artwork src/mkdocs_dsfr/theme/dsfr_assets/
cp -r /tmp/dsfr-$DSFR_VERSION/dist/fonts src/mkdocs_dsfr/theme/dsfr_assets/
cp -r /tmp/dsfr-$DSFR_VERSION/dist/icons src/mkdocs_dsfr/theme/dsfr_assets/
rm -rf /tmp/dsfr-$DSFR_VERSION
sed -i "s/dsfr_version: .*/dsfr_version: \"$DSFR_VERSION\"/" src/mkdocs_dsfr/theme/mkdocs_theme.yml
sed -i "s|\.\./icons|icons|g" src/mkdocs_dsfr/theme/dsfr_assets/utility.css
